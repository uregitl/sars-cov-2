#!/usr/bin/env python
# coding: utf-8

# <a href="https://colab.research.google.com/github/Priesemann-Group/covid19_inference_forecast/blob/master/scripts/example_script_covid19_inference.ipynb" target="_parent"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

# This file create the model with three changing points of the paper https://arxiv.org/abs/2004.01105
# 
# The layout and design of the plots has been simplified in order to make the 
# code more readable and more easily adaptable to other problems.
# 
# ## Installation
# Run this if the module isn't installed yet, or you run it in Google Colab

# In[1]:


#pip install #git+https://github.com/Priesemann-Group/covid19_inference_forecast.git


# #Fit Model

# In[13]:


import sys
import datetime
import pandas as pd
import numpy as np
import pymc3 as pm
import matplotlib.pyplot as plt
import argparse
from argparse import RawTextHelpFormatter
import preprocess as pp

try: 
    import covid19_inference as cov19
except ModuleNotFoundError:
    sys.path.append('..')
    import covid19_inference as cov19

confirmed_cases = cov19.get_jhu_confirmed_cases()

# We need to get population 

p = argparse.ArgumentParser(description = 
                                     '- AE-analysis.py - Extract data from date range and create models -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-c','--country', required= True, help='Force')

#country = 'Italy'
data_df=pd.read_csv("/home/arnee/Desktop/Corona/data/countries_regions.csv",sep=",", encoding = "utf-8")
pp.fix_country_names(data_df)
data_df['date']=data_df.apply(lambda x:pp.FormatDateMerged(x.date), axis=1)
ns = p.parse_args()
country=ns.country

popsize=data_df.loc[data_df.country==ns.country]['popData2018'].mean()
#print (popsize)

date_data_begin = datetime.datetime(2020,3,1) # We used Feb 1 for China
date_data_end   = cov19.get_last_date(confirmed_cases)

#date_data_end   = datetime.datetime(2020,3,28)
num_days_data = (date_data_end-date_data_begin).days
diff_data_sim = 16 # should be significantly larger than the expected delay, in 
                   # order to always fit the same number of data points.
num_days_future = 28
date_begin_sim = date_data_begin - datetime.timedelta(days = diff_data_sim)
date_end_sim   = date_data_end   + datetime.timedelta(days = num_days_future)
num_days_sim = (date_end_sim-date_begin_sim).days


cases_obs = cov19.filter_one_country(confirmed_cases, country,
                                     date_data_begin, date_data_end)

prior_date_mild_dist_begin =  datetime.datetime(2020,3,9)
prior_date_strong_dist_begin =  datetime.datetime(2020,3,16)
prior_date_contact_ban_begin =  datetime.datetime(2020,3,23)

change_points = [dict(pr_mean_date_begin_transient = prior_date_mild_dist_begin,
                      pr_sigma_date_begin_transient = 3,
                      pr_median_lambda = 0.2,
                      pr_sigma_lambda = 0.5),
                 dict(pr_mean_date_begin_transient = prior_date_strong_dist_begin,
                      pr_sigma_date_begin_transient = 1,
                      pr_median_lambda = 1/8,
                      pr_sigma_lambda = 0.5) ,
                 dict(pr_mean_date_begin_transient = prior_date_contact_ban_begin,
                      pr_sigma_date_begin_transient = 1,
                      pr_median_lambda = 1/8/2,
                      pr_sigma_lambda = 0.5)]

model = cov19.SIR_with_change_points(np.diff(cases_obs),
                                        change_points,
                                        date_begin_sim,
                                        num_days_sim,
                                        diff_data_sim,
                                     N=popsize)

trace = pm.sample(model=model, init='advi')


print (model)

# #Plotting

# In[14]:


varnames = cov19.plotting.get_all_free_RVs_names(model)
num_cols = 5
num_rows = int(np.ceil(len(varnames)/num_cols))
x_size = num_cols * 2.5
y_size = num_rows * 2.5

fig, axes = plt.subplots(num_rows, num_cols, figsize = (x_size, y_size),squeeze=False)
i_ax = 0
for i_row, axes_row in enumerate(axes):
    for i_col, ax in enumerate(axes_row):
        if i_ax >= len(varnames):
            ax.set_visible(False)
            continue 
        else:
            cov19.plotting.plot_hist(model, trace, ax, varnames[i_ax], 
                                     colors=('tab:blue', 'tab:green'))
        if not i_col == 0:
            ax.set_ylabel('')
        if i_col == 0 and i_row == 0:
            ax.legend()
        i_ax += 1
fig.subplots_adjust(wspace=0.25, hspace=0.4)


fig.savefig(country+"-1.png")
# In[12]:


fig, axes = cov19.plotting.plot_cases(trace, np.diff(cases_obs), date_begin_sim, diff_data_sim,
                                      colors=('tab:blue', 'tab:green'),country=country) 

fig.savefig(country+"-2.png")

# In[ ]:


#model.to_csv("model-"+country+".csv")
pm.save_trace(trace=trace)
for i in trace.varnames:
    print (i,trace[i].mean(),trace[i].std(),len(trace[i]))

# In[ ]:
model_fpath="model-"+country+".pickle"

with open(model_fpath, 'wb') as buff:
    pickle.dump({'model': model, 'trace': trace, 'X_shared': X_shared}, buff)

#Here X_shared is the Theano shared tensor of the predictor variables.
#Then in my separate “production predict” script I start with

sys.exit()


with open(model_fpath, 'rb') as buff:
    data = pickle.load(buff)
model = data['model']
trace = data['trace']
X_shared = data['X_shared']

X_shared.set_value(np.asarray(X, theano.config.floatX))

#Here X are my predictor variables in production.
#Now I can sample the posterior predictive:

with model:
    post_pred = pm.sample_ppc(trace)

