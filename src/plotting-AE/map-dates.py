#!/usr/bin/env python3

import pandas as pd
import argparse
import branca.colormap as cm
from argparse import RawTextHelpFormatter
import re,sys
import numpy as np
import plotly.express as px
from urllib.request import urlopen
import json
import plotly
import csv
import math
import preprocess as pp
import os
import plotly.io as pio
import geopandas as gpd
from folium.features import GeoJsonPopup
from folium.features import GeoJson
from folium.plugins import TimeSliderChoropleth
import folium

pio.orca.config.use_xvfb = True


def world_map(joined_df, metric,outfile,cmap):
    #temp_df['log_'+metric] = np.log10(temp_df[metric])
    #We can now select the columns needed for the map and discard the others:
    temp_df = joined_df[['country', 'date_sec', metric, 'geometry']]
    #print (cmap)
    #print (temp_df[metric])
    #temp_df.to_csv("foo.csv",sep=",")
    #temp_df['color'] = temp_df[metric].map(cmap)
    #for i,j in temp_df.iterrows():
    #    print (i,j["country"],j["date_sec"],j[metric])
    #    print (cmap(j[metric]))
    temp_df['color'] = temp_df[metric].apply(lambda x:cmap(x))
    country_list = temp_df['country'].drop_duplicates().tolist()
    country_idx = range(len(country_list))
    #create dict with all geometry_df
    style_dict = {}
    for i in country_idx:
        country = country_list[i]
        result = temp_df[temp_df['country'] == country]
        inner_dict = {}
        for _, r in result.iterrows():
            inner_dict[r['date_sec']] = {'color': r['color'], 'opacity': 0.9}
        style_dict[str(i)] = inner_dict
    #select country and geometry coloumns
    countries_df = temp_df[['country','geometry']] #,metric]]  # To include metric makes it very very slow
    countries_gdf = gpd.GeoDataFrame(countries_df)
    countries_gdf = countries_gdf.drop_duplicates().reset_index()
    slider_map = folium.Map(min_zoom=2, max_bounds=True,tiles='cartodbpositron')
    #convert to json file
    data=countries_gdf.to_json()
    print("ok")
    #create TimeSliderChoropleth for dynamic map depend on the time
    _ = TimeSliderChoropleth(
        data=data,
        styledict=style_dict,
        ).add_to(slider_map)
    #other layer for the popup country info
    a=GeoJson(data, popup=(folium.GeoJsonPopup(fields=['country'])),
                style_function=lambda feature: {'fillColor': 'white',
            'color': 'black', 'fillOpacity': 0.1, 'weight': 0.5})
    a.add_to(slider_map)
    _ = cmap.add_to(slider_map)
    slider_map.save(outfile=outfile)


p = argparse.ArgumentParser(description = 
             '- Make a map over R0 values -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= False,
               default="./data",
               help='Input dir formatted CSV file')
p.add_argument('-o','-out','--output_folder',
               default="/home/aure/Desktop/corona",
               required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
if ns.output_folder:
    out_dir = ns.output_folder
# else:
#     out_dir=home = str(Path.home())+"/Desktop/Corona/R-values/"
#     #out_dir=re.sub("/.*","",ns.input)
#     if out_dir == ns.input:
#         out_dir=home = str(Path.home())+"/Desktop/Corona/R-values"
#
# if not os.path.exists(out_dir):
#     print('Creating reports folder...')
#     os.system('mkdir -p ' + out_dir)


geometry_df = gpd.read_file(os.getcwd()+'/data/world+regions.json')
geometry_df = geometry_df.rename(columns={'name': 'country'})
#print (geometry_df.country.drop_duplicates())

concat_df,lastdate,firstdate=pp.get_r0_countries(ns.input)
concatfile=ns.input+"/countries_regions.csv"

#concat_df=concat_df.fillna(0)
concat_df['date_sec'] = pd.to_datetime(concat_df['date']).astype(int) / 10**9
concat_df['date_sec'] = concat_df['date_sec'].astype(int).astype(str)


# We need to set population to something in countries with missing data (set it to 100K)
#concat_df['popData2018']= concat_df.apply(lambda x:max(x['popData2018'],100000),axis=1)
# Just deal with some missing values
concat_df['popData2018'].fillna(100000, inplace=True)
concat_df.R0.fillna(1, inplace=True)
concat_df["new_confirmed_cases"].fillna(0,inplace=True)
concat_df["new_deaths"].fillna(0,inplace=True)
concat_df["deaths"].fillna(0,inplace=True)
concat_df["confirmed"].fillna(0,inplace=True)
concat_df["Ratio"].fillna(0,inplace=True)
concat_df["deaths_7days"].fillna(0,inplace=True)
concat_df["cases_7days"].fillna(0,inplace=True)
# JHS data has sometimes negative numbers
concat_df['deaths_7days']= concat_df.apply(lambda x:max(x['deaths_7days'],0),axis=1)
concat_df['cases_7days']= concat_df.apply(lambda x:max(x['cases_7days'],0),axis=1)


minnum=1

concat_df['deaths_per_million']=1000000*concat_df['deaths']/concat_df['popData2018']
concat_df['cases_per_million']=1000000*concat_df['confirmed']/concat_df['popData2018']
concat_df['Current-deaths_per_million']=1000000*concat_df['deaths_7days']/concat_df['popData2018']
concat_df['Current-cases_per_million']=1000000*concat_df['cases_7days']/concat_df['popData2018']


# We need to to use log to see anything I think 
concat_df['log_deaths'] = np.log10(concat_df['deaths']+minnum)
concat_df['log_cases'] = np.log10(concat_df['confirmed']+minnum)
concat_df['log_deaths_7days'] = np.log10(concat_df['deaths_7days']+minnum)
concat_df['log_cases_7days'] = np.log10(concat_df['cases_7days']+minnum)
concat_df['log_deaths_per_million'] = np.log10(concat_df['deaths_per_million']+minnum)
concat_df['log_cases_per_million'] = np.log10(concat_df['cases_per_million']+minnum)
minnum=0.1
concat_df['log_current_deaths_per_million'] = np.log10(concat_df['Current-deaths_per_million']+minnum)
concat_df['log_current_cases_per_million'] = np.log10(concat_df['Current-cases_per_million']+minnum)
minnum=1
#sys.exit()
#concat_df.to_csv("concat.csv",sep=",")


# Eriks beräkning av dagar kvar

cfr=0.00025  # From Gisecka
herd=0.6
minnum=1

concat_df["days_left"]= concat_df.apply(lambda x:((herd*cfr*x.popData2018-x.deaths)/(x.deaths_7days+minnum)),axis=1)
concat_df["log_days_left"]= concat_df.apply(lambda x:(np.log10(max(x.days_left,minnum))),axis=1)


# Now lets save the files

concat_df.to_csv(concatfile)
#sys.exit()

# Before merging we need to exclude countries where we have regions 
concat_df=concat_df[ (  ( concat_df['country'] != "Spain" )
                     & ( concat_df['country'] != "Sweden" )
                     & ( concat_df['country'] != "United States" )
                     & ( concat_df['country'] != "Italy" )
                     & ( concat_df['country'] != "United States of America" )
                     & ( concat_df['country'] != "USA" )
                     & ( concat_df['country'] != "America" )
                     & ( concat_df['country'] != "China" )
                     & ( concat_df['country'] != "Canada" ))]


joined_df = concat_df.merge(geometry_df, on='country')
'''
max_color = 2
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
#cmap = cm.linear.RdBu.scale(min_color, max_color) # this does not work
metric="R0"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/R0.html",cmap)


#print (min(joined_df["deaths_per_million"]),max(joined_df["deaths_per_million"]))
max_color = max(joined_df["deaths"])
min_color = min(joined_df["deaths"])
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "deaths"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/totaldeaths.html",cmap)



#print (min(joined_df["deaths_per_million"]),max(joined_df["deaths_per_million"]))
max_color = 1500
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/deaths.html",cmap)

max_color = 20000
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/cases.html",cmap)

max_color = 25
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "Current-deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/currentdeaths.html",cmap)

max_color = 500
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "Current-cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/currentcases.html",cmap)


#print (min(joined_df["deaths_per_million"]),max(joined_df["deaths_per_million"]))
max_color = np.log10(1500)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_deaths.html",cmap)

max_color = np.log10(20000)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_cases.html",cmap)

minnum=0.1
max_color = np.log10(25)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_current_deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_currentdeaths.html",cmap)
'''
max_color = np.log10(500)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_current_cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_currentcases.html",cmap)
'''

max_color = 3650
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "days_left"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/days_left.html",cmap)

#joined_df.to_csv("joined.csv",sep=",")


max_color = np.log10(3650)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_days_left"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_days_left.html",cmap)


# Try to plot mobility, but we need to delete rows with empty values


max_color = +100
min_color = -100
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
for c in ["retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential"]:
    joined_df=joined_df.loc[joined_df[c].notna()]
    print (c)
    metric = c 
    cmap.caption = metric
    #joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
    #joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
    world_map(joined_df,metric,out_dir+c+".html",cmap)


'''

