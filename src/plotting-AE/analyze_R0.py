#!/usr/bin/env python3
from __future__ import print_function
import pandas as pd
import numpy as np
import os
import sys
from pathlib import Path
import argparse
from argparse import RawTextHelpFormatter
import re
from dateutil.parser import parse
from datetime import datetime,date,time, timedelta
from dateutil import parser
#import pyarrow
import matplotlib.pyplot as plt
import matplotlib as mpl
# %matplotlib inline
from scipy.stats import linregress
from scipy.optimize import curve_fit
import preprocess as pp
import seaborn as sns
import sys
import codecs
#sys.stdout = codecs.getwriter('utf8')(sys.stdout)
#sys.stderr = codecs.getwriter('utf8')(sys.stderr)
#sys.stdout.buffer.write(TestText2)
mpl.rc('figure', max_open_warning = 0)
sns.set()
sns.set(font_scale=1.4)


def pop_std(x):
    return x.std(ddof=0)

def plot_time(df,outname,name,columns): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    extra=["new_deaths","new_confirmed_cases"]
    fig, ax1 = plt.subplots(figsize=(16,10))
    #tmp_df=df.loc[df.date>=startdate]
    tmp_df=df.groupby(['date'])[columns+extra].mean()
    std_df=df.groupby(['date'])[columns+extra].std()
    N_df=df.groupby(['date'])[columns+extra].size()
    N_df=N_df.apply(lambda x: np.sqrt(x))
    stderr_df=pd.DataFrame([])
    lower_bound=pd.DataFrame([])
    higher_bound=pd.DataFrame([])
    lower_bound25=pd.DataFrame([])
    higher_bound75=pd.DataFrame([])

    for c in columns+extra:
        stderr_df[c]=std_df[c]/N_df
        higher_bound[c]=tmp_df[c]+1.96*stderr_df[c]
        lower_bound[c]=tmp_df[c]-1.96*stderr_df[c]
        higher_bound75[c]=tmp_df[c]+1.15*stderr_df[c]
        lower_bound25[c]=tmp_df[c]-1.15*stderr_df[c]


    tmp_df["date"]=df.groupby(['date'])["date"].first()
    #tmp_df=df
    #print (tmp_df)
    x=np.arange(0,len(tmp_df))
    #print (tmp_df)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    shift=-0.2
    for c in extra:
        ax2.bar(x+shift,tmp_df[c],yerr=stderr_df[c], alpha = 0.5,width=0.4,label=c)
        shift+=0.4
        
    ax2.set_ylabel('No. Cases /deaths',color='r')
    ax2.tick_params(axis='y', labelcolor='r')
    ax2.legend
    for c in columns:
        y=tmp_df[c]
        yerr=std_df[c]
        ax1.plot(x,y,label=c, linewidth = 1.0)
        ax1.fill_between(x, lower_bound[c], higher_bound[c], color='cornflowerblue', alpha=0.4)
        ax1.fill_between(x, lower_bound25[c], higher_bound75[c], color='cornflowerblue', alpha=0.6)
    dates = tmp_df['date'].to_list()
    #ax1.legend(loc='lower left', frameon=False, markerscale=2)
    ax1.set_ylabel('Value')
    #ax1.set_yscale('log')
    ax1.set(title="R0 and mobility "+ name )
    ax1.grid(color='b',linestyle="-",linewidth=1)
    ax1.tick_params(axis='x', labelrotation=45 )    
    #ax1.set_ylim([0,max(higher_bound)])
    xticks=np.arange(0,len(tmp_df),7)
    #yticks=[0.1,0.5,1,2,5,10]
    
    #ax1.set_xticklabels(dates)
    #plt.xticks(rotation=45, ha='right')
    ax1.set_xticklabels(dates[0::7],rotation=45)
    #ax1.set_yticklabels(["0.1","0.5","1","2","5","10"])
    ax1.set_xticks(xticks)
    #ax1.set_yticks(yticks)
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.tight_layout()
    ax1.legend()
    fig.savefig(outname, format = 'png', dpi=300)
    plt.close(fig="all")

    
def plot_scatter(df,outname,name,X,Y): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    tmp_df=df
    sns_plot = sns.jointplot(x=tmp_df[X],y=tmp_df[Y], kind='kde')
    sns_plot.savefig(outname, format = 'png', dpi=300)
    plt.close(fig="all")
    
def plot_countries(df1,df2,outname,name,key): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    tmp_df=df1.merge(df2,on="date")
    sns_plot = sns.jointplot(x=tmp_df[key+"_x"],y=tmp_df[key+"_y"], kind='reg')
    sns_plot.savefig(outname, format = 'png', dpi=300)
    plt.close(fig="all")
    #sys.exit()
def plot_countries2(df1,df2,outname,name,key): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    tmp_df=df1.merge(df2,on="date")
    tmp_df['num'] = np.arange(len(tmp_df))
    sns_plot = sns.relplot(x=key+"_x",y=key+"_y",  size="num", hue="num"  ,
            sizes=(40, 400), alpha=.5, palette="YlOrRd",
                           height=6,data=tmp_df,legend=False)    
    sns_plot.savefig(outname, format = 'png', dpi=300)
    plt.close(fig="all")
    #sys.exit()
    
def plot_scatter2(df,outname,name,X,Y): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_boun2d25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    fig, ax1 = plt.subplots(figsize=(16,10))

    plt.scatter(tmp_df[X],
            tmp_df[Y],       # <== 😀 2nd DIMENSION
            #s=size,                 # <== 😀 3rd DIMENSION
            color=fill_colors,      # <== 😀 4th DIMENSION             
            edgecolors=edge_colors,
            alpha=0.4)
    sns_plot.savefig(outname, format = 'png', dpi=300)
    plt.close()

p = argparse.ArgumentParser(description = 
                                     '- AE-analysis.py - Extract data from date range and create models -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-f','--force', required= False, help='Force', action='store_true')
p.add_argument('-o','-out','--output_folder', required= True, help='output folder')
p.add_argument('-data','--input','-i', required= True, help='Input formatted CSV file')
p.add_argument('-countries','-c', required= False, help='Only include selected countries from config', nargs='+')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()

out_dir = ns.output_folder
if not os.path.exists(out_dir):
    print('Creating output folder...')
    os.system('mkdir -p ' + out_dir)
        
data_df=pd.read_csv(ns.input,sep=",", encoding = "utf-8")
pp.fix_country_names(data_df)
data_df['date']=data_df.apply(lambda x:pp.FormatDateMerged(x.date), axis=1)


mincases=8000  # 1000  makes us include New Zealand, 8000 Norway, 10000 South Korea

# Delete all countries with no moblity data and to few cases

all_df=data_df.groupby(['country']).max().reset_index()
all=all_df.country.drop_duplicates()

regions_df=all_df.loc[all_df.datasource.isin(["Sweden","Italy","Spain","JHS-regions"])].reset_index()
regions=regions_df.country.drop_duplicates()
countries_df=all_df.loc[all_df.datasource.isin(["ECDC"])].reset_index()
countries=countries_df.country.drop_duplicates()


big_df=all_df.loc[all_df.grocery_and_pharmacy.notna() ].reset_index()
big_df=big_df.loc[big_df.confirmed>mincases].reset_index()
big=big_df.country.drop_duplicates()

bigcountries_df=countries_df.loc[(countries_df.grocery_and_pharmacy.notna()) &
                                 (countries_df.confirmed>mincases )].reset_index()
bigcountries=bigcountries_df.country.drop_duplicates()

bigregions_df=regions_df.loc[(regions_df.grocery_and_pharmacy.notna()) &
                                 (regions_df.confirmed>mincases )].reset_index()
bigregions=bigregions_df.country.drop_duplicates()

#print(country_df,countries)

# 

    
dataset=bigcountries
dataset=["Sweden","Spain","Italy","Belgium","Germany","UK","Denmark","Norway"]
firstdate=datetime.strptime("2020-03-01","%Y-%m-%d")

data_df=data_df.loc[(data_df.country.isin(dataset)) &
                    (data_df.date>=firstdate)]

# We should skip all dates with less than X Cummulative cases)


# Now we need to fix mobility before and after the first dates

mobilitycolumns=["retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential"]


    
columns=["country","R0","new_confirmed_cases","new_deaths","date","deaths","confirmed","Ratio","deaths_7days","cases_7days","popData2018","code","retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential","date_sec","deaths/million","cases/million","Current deaths/million","Current cases/million","log_deaths","log_cases","log_deaths_7days","log_cases_7days","log_deaths/million","log_cases/million","log_current_deaths/million","log_current_cases/million","days_left","log_days_left"]

data_df.rename(columns={
    "deaths/million":"deaths_per_million",
    "cases/million":"cases_per_million",
    "Current deaths/million":"Current-deaths_per_million",
    "Current cases/million":"Current-cases_per_million",
    "log_deaths":"log_deaths",
    "log_deaths/million":"log_deaths_per_million",
    "log_cases/million":"log_cases_per_million",
    "log_current_deaths/million":"log_current_deaths_per_million",
    "log_current_cases/million":"log_current_cases_per_million",
 },inplace=True)


#print (country_df)
#country_df.to_csv("country.csv")

merged_df = pd.DataFrame([])


for country in dataset:
    temp_df=data_df.loc[data_df.country==country]
    for key in mobilitycolumns:
        firstpos=temp_df[key].notna().idxmax()
        temp_df[key].loc[:firstpos-1]=1
        lastpos=temp_df[key].isna().idxmax()
        lastvalue=temp_df[key].loc[lastpos-8:lastpos-1].mean()
        temp_df[key].loc[lastpos:]=lastvalue

    for key in mobilitycolumns:
        # First we set it all to 1 until the first date

        # Then we do a rolling mean
        temp_df[key+'_7days']=temp_df[key].rolling(7).mean()

    # Finally we add the first 6 days.
    for key in mobilitycolumns:
        firstpos=temp_df[key+'_7days'].notna().idxmax()
        firstvalue=temp_df[key+'_7days'].loc[firstpos]
        temp_df[key+'_7days'].loc[:firstpos-1]=firstvalue

    merged_df=pd.concat([merged_df, temp_df], ignore_index=True,sort=False)
        # 

#print (merged_df)
# Handle sudden spikes in cases/deaths (should be done before calculating R0)        
merged_df.to_csv("merged.csv",sep=",")
#merged_df=pd.read_csv("merged.csv",sep=",")
print (merged_df)

# We need to deal with some too extreme values
tiny=0.1
minR=tiny
maxR=50000
merged_df["R0"]=merged_df["R0"].apply(lambda x:max(minR,(min(x,maxR))))
#minDeath=0
#maxDeath=1000
#merged_df["new_deaths"]=merged_df["new_deaths"].apply(lambda x:max(minDeath,(min(x,maxDeath))))
minCases=0
maxCases=10000
merged_df["new_confirmed_cases"]=merged_df["new_confirmed_cases"].apply(lambda x:max(minCases,(min(x,maxCases))))


# Plottar 

# R0 vs mobility
#Multiple R0 with 100

tiny=0.1
maxR=3
merged_df["100*R0"]=merged_df["R0"].apply(lambda x:(min(x*100,maxR*100)))
merged_df['25*LogR0']=merged_df['R0'].apply(lambda x:(25*np.log10(max(x,tiny))))

plotcolumns=["25*LogR0","R0"] # ,"100*R0"]
for key in mobilitycolumns:
    plotcolumns+=[key+"_7days"]

scattercolumns=["new_confirmed_cases","new_deaths","Current_cases_per_million","Current_deaths_per_million"]
for key in mobilitycolumns:
    scattercolumns+=[key+"_7days"]
    
#for key in scattercolumns:
#    plot_scatter(merged_df,out_dir+"/R0_vs-mobility-"+key+".png","All","R0",key)
        
#print (merged_df)    
for country in countries:
    temp_df=merged_df[merged_df['country'] == country]
    #temp_df=merged_df.loc[(merged_df.country==country)] #  & (merged_df.country.notna())]
    #print (country,temp_df)
    plot_time(temp_df,out_dir+"/R0-mobility-"+country+".png",country,plotcolumns)
    for key in scattercolumns:
        plot_scatter(temp_df,out_dir+"/R0_vs-mobility-"+country+"-"+key+".png",country,"R0",key)

merged_df["100*R0"]=merged_df["R0"].apply(lambda x:x*100)
merged_df['LogR0']=merged_df['R0'].apply(lambda x:(np.log10(max(x,tiny))))

plotcolumns=["LogR0","R0"] # ,"100*R0"]
for key in mobilitycolumns:
    plotcolumns+=[key+"_7days"]

scattercolumns=["new_confirmed_cases","new_deaths","Current-cases_per_million","Current-deaths_per_million"]
for key in mobilitycolumns:
    scattercolumns+=[key+"_7days"]

minconfirmed=1000    
temp_df=merged_df.loc[merged_df.confirmed>=minconfirmed]

        

            
outname=out_dir+"/R0-mobility-All.png"
if (not os.path.exists(outname) or ns.force):
    plot_time(merged_df,outname,"All",plotcolumns)
    outname=out_dir+"/R0-mobility-All-minconfirmed.png"
    plot_time(temp_df,outname,"All min:"+str(minconfirmed),plotcolumns)
    outname=out_dir+"/R0-All.png"
    plot_time(temp_df,outname,"All",["R0"])
    outname=out_dir+"/R0-All-minconfirmed.png"
    plot_time(temp_df,outname,"All min:"+str(minconfirmed),["R0"])
    outname=out_dir+"/R0-All-log.png"
    plot_time(temp_df,outname,"All",["LogR0"])
    outname=out_dir+"/LogR0-All-minconfirmed.png"
    plot_time(temp_df,outname,"All min:"+str(minconfirmed),["LogR0"])
        
for key in scattercolumns:
    outname=out_dir+"/R0_vs-mobility-All-"+key+".png"
    if (not os.path.exists(outname) or ns.force):
        plot_scatter(merged_df,outname,"All","R0",key)

    outname=out_dir+"/R0_vs-mobility-All-minconfirmed-"+key+".png"
    if (not os.path.exists(outname) or ns.force):
        plot_scatter(temp_df,outname,"All min:"+str(minconfirmed),"R0",key)

    outname=out_dir+"/LogR0_vs-mobility-All-"+key+".png"
    if (not os.path.exists(outname) or ns.force):
        plot_scatter(merged_df,outname,"All","LogR0",key)

    outname=out_dir+"/LogR0_vs-mobility-All-minconfirmed-"+key+".png"
    if (not os.path.exists(outname) or ns.force):
        plot_scatter(temp_df,outname,"All min:"+str(minconfirmed),"LogR0",key)

#sys.exit()

#print (merged_df)    
for country in dataset:
    temp_df=merged_df[merged_df['country'] == country]
    #temp_df=merged_df.loc[(merged_df.country==country)] #  & (merged_df.country.notna())]
    #print (country,temp_df)
    outname=out_dir+"/R0-mobility-"+country+".png"
    if (not os.path.exists(outname) or ns.force):
        plot_time(temp_df,outname,country,plotcolumns)
    for key in scattercolumns:
        outname=out_dir+"/R0_vs-mobility-"+country+"-"+key+".png"
        if (not os.path.exists(outname) or ns.force):
            plot_scatter(temp_df,outname,country,"R0",key)

    
#print (merged_df)    
for c in dataset:
    c_df=merged_df[merged_df['country'] == c]
    for d in dataset:
        if c==d:
            continue
        d_df=merged_df[merged_df['country'] == d]
        outname=out_dir+"/LogR0-"+c+"-"+d+".png"
        outname2=out_dir+"/LogR0-relpot-"+c+"-"+d+".png"
        if (not os.path.exists(outname) or ns.force):
            plot_countries(c_df,d_df,outname,"R0 in "+c+" vs "+d,"LogR0")
            plot_countries2(c_df,d_df,outname2,"R0 in "+c+" vs "+d,"LogR0")
