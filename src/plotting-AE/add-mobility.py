#!/usr/bin/env python3

from __future__ import print_function
import pandas as pd
import numpy as np
import argparse
from argparse import RawTextHelpFormatter
from datetime import datetime,date,time, timedelta
import re
import preprocess as pp
import os



p = argparse.ArgumentParser(description = 
                                     '- AE-analysis.py - Extract data from date range and create models -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input formatted CSV file')
#p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()




input_df=pd.read_csv(ns.input, sep=',')
#print (merged_df.country.drop_duplicates())
pp.fix_regions(input_df)
pp.italy_regions(input_df)
pp.spain_communities(input_df)
global_df = pd.read_csv(os.getcwd()+'/data/Global_Mobility_Report.csv', sep=',')
#print (global_df.columns)
# Remove all counties.
global_df=global_df.loc[global_df.sub_region_2.isna()]

global_df.sub_region_1.fillna('XXXX',inplace=True)
global_df["country"] = global_df.country_region+"-"+global_df.sub_region_1
global_df.replace(['United States-'],['US-'],regex=True,inplace=True)
global_df.replace(['US-XXXX'],['USA'],regex=True,inplace=True)
global_df.replace(['China-'],['CH-'],regex=True,inplace=True)
global_df.replace(['Ch-XXXX'],['China'],regex=True,inplace=True)
global_df.replace(['Sweden-XXXX'],['Sweden'],regex=True,inplace=True)
global_df.replace(['Sweden-'],[''],regex=True,inplace=True)
global_df.replace(['Spain-XXXX'],['Spain'],regex=True,inplace=True)
global_df.replace(['Spain-'],[''],regex=True,inplace=True)
global_df.replace(['Italy-XXXX'],['Italy'],regex=True,inplace=True)
global_df.replace(['Italy-'],[''],regex=True,inplace=True)
global_df.replace(['Canada-XXXX'],['Canada'],regex=True,inplace=True)
global_df.replace(['Canada-'],['CA-'],regex=True,inplace=True)
global_df.replace(['Hong_Kong-Hong_Kong'],['CH-Hong Kong'],regex=True,inplace=True)
global_df.replace(['Andalusia'],['Andalucia'],regex=True,inplace=True)
global_df.replace(['-XXXX'],[''],regex=True,inplace=True)

pp.fix_country_names(global_df)
# We need to map names
#print (input_df.country.drop_duplicates())
#print (global_df.country.drop_duplicates())
inp=pd.DataFrame(input_df.country.drop_duplicates())
inp.to_csv("input.csv")
glob=pd.DataFrame(global_df.country.drop_duplicates())
glob.to_csv("global.csv")


merged_df=pd.merge(input_df, global_df, how='left', on=['country','date'])
#columns=["new_confirmed_cases","new_deaths","country","popData2018","date","deaths","confirmed","Days","DeathsDays","LogCases","LogDeaths","Ratio","LinCases","LinDeaths","retail_and_recreation_percent_change_from_baseline","grocery_and_pharmacy_percent_change_from_baseline","parks_percent_change_from_baseline","transit_stations_percent_change_from_baseline","workplaces_percent_change_from_baseline","residential_percent_change_from_baseline"]
#merged_df=merged_df[columns]

merged_df=merged_df.rename(columns={
        "retail_and_recreation_percent_change_from_baseline":"retail_and_recreation",
        "grocery_and_pharmacy_percent_change_from_baseline":"grocery_and_pharmacy",
        "parks_percent_change_from_baseline":"parks",
        "transit_stations_percent_change_from_baseline":"transit_stations",
        "workplaces_percent_change_from_baseline":"workplaces",
        "residential_percent_change_from_baseline":"residential"
        })


outfile=re.sub(r'.csv','',ns.input)+"-mobility.csv"

merged_df.to_csv("merged.csv")
merged_df.to_csv(outfile)

# We need to print the missing fields

nomobility_df=pd.DataFrame(merged_df.loc[
    ((merged_df.workplaces.isna())&
    (merged_df.date=="2020-05-01"))
    ]["country"].drop_duplicates())
nomobility_df.to_csv("missing.csv")

