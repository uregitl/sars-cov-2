#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

if (length(args)!=3) {
  stop("Three argument must be supplied (input file).n", call.=FALSE)
}

inp=args[1]
out=args[2]
outB=b=gsub(".csv","-fixedSI.csv",out)
filt=args[3]

library(EpiEstim)
library(dplyr)
#setwd('/home/arnee/Desktop/Corona/data/')

data<-read.csv(inp)
country<- data %>% filter(data$country == filt)
cases<-pmax(country$new_confirmed_cases,0)

R_estimate <- EpiEstim::estimate_R(cases,  method="uncertain_si",
                                   config = make_config(list(mean_si = 7.5, std_mean_si = 2,
                                                             min_mean_si = 1, max_mean_si = 8.4,
                                                             std_si = 3.4, std_std_si = 1, min_std_si = 0.5,
                                                             max_std_si = 4, n1 = 1000, n2 = 1000)))
write.csv(R_estimate$R,out)


R2_estimate <- EpiEstim::estimate_R(cases,  method = "parametric_si", config = make_config(list(mean_si = 4.8,
                                                                                               std_si = 2.3)))
write.csv(R2_estimate$R,outB)

# Let's also estimate using FHMs values (mean_si = 4.8, std_si = 2.3), 


                                        #disease_incidence_data <- read.csv("DataS1.csv", header = FALSE)
#serial_interval_data <- read.csv("DataS2.csv", header = FALSE)
#names <- c("EL", "ER", "SL", "SR")
#colnames(serial_interval_data) <- names
#serial_interval_data <- as.data.frame(serial_interval_data)
#R_estimate <- EpiEstim::estimate_R(disease_incidence_data, si_data = serial_interval_data, method="si_from_data", config=list(t_start=2:9, t_end=7:14, n1 = 500, n2 = 100, seed = 1, mcmc_control=list(init.pars=NULL, burnin=3000, thin=10, seed=1), si_parametric_distr = "off1G"))
#SL = c(5, 9, 7, 3, 7, 8, 1, 3, 7, 9, 12)
#si_data <- data.frame(EL = as.integer(rep(0, 11)), ER = as.integer(rep(1,     11)), SL = as.integer(SL), SR = as.integer(SL + 1))
#R_estimate <- EpiEstim::estimate_R(Infected, si_data = si_data, method="si_from_data", config=list(t_start=2:9, t_end=7:14, n1 = 500, n2 = 100, seed = 1, mcmc_control=list(init.pars=NULL, burnin=3000, thin=10, seed=1), si_parametric_distr = "off1G"))

#za
#                                        # Was  R_estimate <- estimate_r(disease_incidence_data, si_data = serial_interval_data, method="si_from_data", config=list(t_start=2:9, t_end=7:14, n1 = 500, n2 = 100, seed = 1, mcmc_control=list(init.pars=NULL, burnin=3000, thin=10, seed=1), si_parametric_distr = "off1G", plot=TRUE))
#R_estimate$R
#SIR <- function(time, state, parameters) {
#    par <- as.list(c(state, parameters))
#    with(par, {
#        dS <- -beta * I * S/N
#        dI <- beta * I * S/N - gamma * I
#        dR <- gamma * I
#        list(c(dS, dI, dR))
#    })
#}
#library(lubridate)
#FHM$FixedDate <- as.Date(FHM$date,format="%Y-%m-%d")
#Infected <- FHM %>% filter(FHM$country == "Stockholm",     FHM$FixedDate >= ymd("2020-02-20"), FHM$FixedDate <= ymd("2020-03-15")) %>%     pull(confirmed)
#library(deSolve)
#RSS <- function(parameters) {
#    names(parameters) <- c("beta", "gamma")
#    out <- ode(y = init, times = Day, func = SIR, parms = parameters)
#    fit <- out[, 3]
#    sum((Infected - fit)^2)
#}
#N<-2000000
#init <- c(S = N - Infected[1], I = Infected[1], R = 0)
