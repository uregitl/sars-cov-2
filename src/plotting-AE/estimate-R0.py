#!/usr/bin/env python3

from __future__ import print_function
import pandas as pd
import numpy as np
import argparse
from argparse import RawTextHelpFormatter
import re
from datetime import datetime,date,time, timedelta

import matplotlib.pyplot as plt
import matplotlib as mpl
import preprocess as pp
import os
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
mpl.rc('figure', max_open_warning = 0)

def plot_shade(df,outname,country,startdate): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    tmp_df=df.loc[df.date>=startdate]
    y=tmp_df['Mean(R)']
    x=np.arange(0,len(y))
    lower_bound=tmp_df['Quantile.0.05(R)']
    higher_bound=tmp_df['Quantile.0.95(R)']
    lower_bound25=tmp_df['Quantile.0.25(R)']
    higher_bound75=tmp_df['Quantile.0.75(R)']
    dates = tmp_df['date'].to_list()
    cases= tmp_df['new_confirmed_cases']
    #fig, ax1 = plt.subplots(figsize=(8/2.54, 5/2.54))
    fig, ax1 = plt.subplots(figsize=(16,10))
    #Plot observed dates
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    if len(cases)>1:
        ax2.bar(x,cases, alpha = 0.5,color='r') #3 week forecast'
        ax2.set_ylabel('No. Cases',color='r')
        ax2.tick_params(axis='y', labelcolor='r')
    ax1.plot(x,y, alpha=0.5, color='b', label='R', linewidth = 1.0)
    ax1.fill_between(x, lower_bound, higher_bound, color='cornflowerblue', alpha=0.4)
    ax1.fill_between(x, lower_bound25, higher_bound75, color='cornflowerblue', alpha=0.6)
    #ax1.legend(loc='lower left', frameon=False, markerscale=2)
    ax1.set_ylabel('R0')
    ax1.set_yscale('log')
    ax1.set(title="R0 estimated for "+ country + " Using EpiEstem and flexible SI")
    ax1.grid()
    ax1.tick_params(axis='x', labelrotation=45 )    
    #ax1.set_ylim([0,max(higher_bound)])
    xticks=np.arange(0,len(y),7)
    yticks=[0.1,0.5,1,2,5,10]
    
    #ax1.set_xticklabels(dates)
    #plt.xticks(rotation=45, ha='right')
    ax1.set_xticklabels(dates[0::7],rotation=45)
    ax1.set_yticklabels(["0.1","0.5","1","2","5","10"])
    ax1.set_xticks(xticks)
    ax1.set_yticks(yticks)
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.tight_layout()

    fig.savefig(outname, format = 'png', dpi=300)
    plt.close()
    
mpl.rcParams.update({'font.size': 22})

Rscript="Rscript ./estimate-R0.R"

p = argparse.ArgumentParser(description = 
                                     '- AE-analysis.py - Extract data from date range and create models -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input formatted CSV file')
p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
#mpl.rc('figure', max_open_warning = 0)
#mpl.rcParams.update({'figure.autolayout': True})

if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=home = str(Path.home())+"/Desktop/Corona/R-values/"
    #out_dir=re.sub("/.*","",ns.input)
    if out_dir == ns.input:
        out_dir=home = str(Path.home())+"/Desktop/Corona/R-values"        


if not os.path.exists(out_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + out_dir)
    



merged_df=pd.read_csv(ns.input, sep=',')
#print (merged_df.country.drop_duplicates())
pp.fix_country_names(merged_df)

#print (merged_df.country.drop_duplicates())

countries=merged_df['country'].drop_duplicates()
#columns=["DateRep","day","month","year","new_confirmed_cases","new_deaths","country","geoId","countryterritoryCode","popData2018","continentExp","date","deaths","confirmed","Days","DeathsDays","LogCases","LogDeaths","Ratio","LinCases","LinDeaths"]
columns=["new_confirmed_cases","new_deaths","countryterritoryCode","popData2018","continentExp","date","deaths","confirmed","Ratio"]



latest={}
#latest["R0"]={}
#countries=["Sweden","Spain"]
#countries=["Abruzzo","P.A._Bolzano"]
for country in countries:
    minnum=10
    print (country)
    outfile=re.sub(".csv","-",ns.input)+country+".csv"
    outfile2=re.sub(".csv","-",ns.input)+country+"-merged.csv"
    fixedSI=re.sub(".csv","-",ns.input)+country+"-fixedSI.csv"
    #outplot=re.sub(".csv","-",ns.input)+country+".png"
    outplot=out_dir+"/R-value-"+country+".png"
    #print (ns.input,outfile,country)
    # Now make some simple plots
    temp_df=merged_df[merged_df.country==country].copy()
    if temp_df.confirmed.max()<=minnum:
        minnum=0
        if temp_df.confirmed.max()==0:
            continue
    try:
        print (Rscript + " " + ns.input + " " +outfile+ " " + country)
        os.system(Rscript + " " + ns.input + " " +outfile+ " " + country)
    except:
        print ("Warning: estimate of R0 crashed for: ",country)
        continue
        #foo_df=temp_df.copy()
    try:
        R_df=pd.read_csv(outfile,sep=',')
        R2_df=pd.read_csv(fixedSI,sep=',')
        R_df["R0-FixedSI"]=R2_df["Mean(R)"]
        R_df["Std-R0-FixedSI"]=R2_df["Std(R)"]
    except:
        print ("Warning: could not read CSV file for: ",country)
        continue
    temp_df.sort_index(inplace=True)
    temp_df.drop(temp_df.index[[0,1,2,3]],inplace=True)
    #temp_df.drop(temp_df.index[[-3,-2,-1]],inplace=True)
    
    R_df.reset_index(inplace=True) 
    temp_df.reset_index(inplace=True) 
    col_R=R_df.columns.to_list()
    col_M=temp_df.columns.to_list()
    cols=col_R+col_M
    joined_df=pd.concat([R_df, temp_df], axis=1, sort=False,ignore_index=True)
    joined_df.columns=cols
    #print (joined_df)
    joined_df['daterep']=joined_df.apply(lambda x:pp.FormatDateMerged(x.date), axis=1)

    fig, ax = plt.subplots(figsize=(20,10))
    startdate=joined_df[joined_df.confirmed > minnum].iloc[0].date
    plot_shade(joined_df,outplot,country,startdate) #,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)plot_sh
    #print (R_df["Mean(R)"],latest)
    #latest[country]={}
    #print (joined_df.daterep)
    for date in joined_df.daterep.drop_duplicates():
        oneweekago=date-timedelta(7)
        #date_df=joined_df.loc[joined_df.daterep==date]
        key=country+str(date.date())
        latest[key]={}
        latest[key]["country"]=country
        latest[key]["daterep"]=date
        #print (key,date,joined_df[joined_df.daterep==date]["Mean(R)"])
        latest[key]["R0"]=joined_df[joined_df.daterep==date]["Mean(R)"].iloc(0)[0]
        latest[key]["R0-FixSI"]=joined_df[joined_df.daterep==date]["R0-FixedSI"].iloc(0)[0]
        latest[key]["sdR0"]=joined_df[joined_df.daterep==date]["Std(R)"].iloc(0)[0]
        latest[key]["sdR0-FixSI"]=joined_df[joined_df.daterep==date]["Std-R0-FixedSI"].iloc(0)[0]
        for c in columns:
            if c in joined_df:
                latest[key][c]=joined_df[joined_df.daterep==date][c].iloc(0)[0]
        latest[key]['deaths_7days']=joined_df[(joined_df.daterep<=date) & (joined_df.daterep>oneweekago) ]['new_deaths'].mean()
        latest[key]['cases_7days']=joined_df[(joined_df.daterep<=date) & (joined_df.daterep>oneweekago) ]['new_confirmed_cases'].mean()
    #def plot_shade_ci(x,end,start_date,y, observed_y, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname,country_npi, country_retail, country_grocery, country_transit, country_work, country_residential, short_dates):
    joined_df.to_csv(outfile2,sep=',')

    # Model from https://timchurches.github.io/blog/posts/2020-02-18-analysing-covid-19-2019-ncov-outbreak-data-with-r-part-1/
summary_df=pd.DataFrame.from_dict(latest).transpose()
#summary_df['deaths_7days'] = summary_df["new_deaths"].rolling(7).mean()
#summary_df['cases_7days'] = summary_df["new_confirmed_cases"].rolling(7).mean()
outfile3=re.sub(r'.csv','',ns.input)+"-R0.csv"
summary_df.to_csv(outfile3,sep=',')

