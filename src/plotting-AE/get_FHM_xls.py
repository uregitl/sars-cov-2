#!/usr/bin/env python3
from __future__ import print_function
import pandas as pd
import numpy as np
import os
import sys, traceback
from pathlib import Path
import argparse
from argparse import RawTextHelpFormatter
import re
import glob
import math
import wget
import docopt
#import pickle
import os.path
import operator
from dominate import document
from dominate.tags import *

from dateutil.parser import parse
from datetime import datetime,date,time, timedelta
from dateutil import parser
#import pyarrow
#import matplotlib.pyplot as plt
#import matplotlib as mpl
# %matplotlib inline
from scipy.stats import linregress
from scipy.optimize import curve_fit

#mpl.rc('figure', max_open_warning = 0)

import preprocess as pp
import config as cf

datafile="merged_FHM_xls.csv"

##args = docopt.docopt(__doc__)
#out_dir = args['--output_folder']


p = argparse.ArgumentParser(description =  '- get_FHM.py - Extract data from FHM and format it to merged.csv -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-f','--force', required= False, help='Force', action='store_true')
p.add_argument('-out','--output_folder', required= False, help='output folder')
ns = p.parse_args()

if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=str(Path.home())+"/Desktop/Corona/"

# Dynamic parameters
data_dir  = os.path.join(out_dir,'data') # os.path.join(out, 'data'  )+"/" # , str(datetime.date(datetime.now())))
# Possibly we need to get a new link from here:
# https://www.folkhalsomyndigheten.se/smittskydd-beredskap/utbrott/aktuella-utbrott/covid-19/bekraftade-fall-i-sverige/
#FHM="https://www.arcgis.com/sharing/rest/content/items/b5e7488e117749c19881cce45db13f7e/data"
#FHM = "https://www.ecdc.europa.eu/sites/default/files/documents/"  # +2020-03-20+".xlsx
FHM=str(Path.home())+"/Documents/FHM-data/data/FHM/"
# import data
image_dir =  out_dir #os.path.join(out,'reports', 'images')
nation_dir =  os.path.join(out_dir,'nations')
reports_dir = os.path.join(out_dir,'data')

if not os.path.exists(image_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + image_dir)
if not os.path.exists(nation_dir):
    print('Creating nation folder...')
    os.system('mkdir -p ' + nation_dir)
if not os.path.exists(data_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + data_dir)
if not os.path.exists(reports_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + reports_dir)

today=date.today()
yesterday=date.today() - timedelta(1)
aweekago=date.today() - timedelta(7)




# First we test if we already run the data for today:
try:
    merged_df=pd.read_csv(reports_dir+"/"+datafile, sep=',')
    date=merged_df['date'].max()
except:
    ns.force=True
    date=yesterday
if str(date)==str(today) and (not ns.force):
    print ("Exiting as todays plots are alredy run, use --force to rerun")
    sys.exit(0)


excelfile="Folkhalsomyndigheten_Covid19-"+str(today)+".xlsx"
infile=FHM+"/"+excelfile

#print(infile)
if not os.path.isfile(infile):
    if (not ns.force) and str(date)!=str(yesterday):
        print ("Exiting as there is no new data yet, use --force to rerun on yesterdays data")
        sys.exit(0)
    else:    
        print ("Rerunning using yesterdays data")
        
# First we test if we already run the data for today:

    
print('Importing Data...')
print("Using: ",FHM)
# We need to read all the excel files.
sheet="Totalt antal per region"
sheet_names=[]
df_date={}
#df_iva={}
#df_death={}
for f in os.listdir(FHM):
    if (f.find("._")!= -1):  # This is to avoid files open by excel
        continue
    if (f.find("~")!= -1):  # This is to avoid files open by excel
        continue
    if (f.find("latest")== -1):
        date=re.sub(r'.xlsx','',re.sub(r'.*Covid19_','',f))
        xls = pd.ExcelFile(FHM+f)
        df_date[date] = xls.parse(sheet)
        #df_iva[date] = xls.parse("Antal intensivvårdade per dag")
        #df_death[date] = xls.parse("Antal avlidna per dag")
    else:
        df_map={}
        xls = pd.ExcelFile(FHM+f)
        sheet_names=xls.sheet_names
        for sheet_name in xls.sheet_names:
            df_map[sheet_name]=xls.parse(sheet_name)



latest_df=df_map["Antal per dag region"].fillna(0)
latest_df=latest_df.rename(columns={
    'Jämtland_Härjedalen':'Jämtland Härjedalen',
    'Västra_Götaland':'Västra Götaland'
    })

dead_dict={}
iva_dict={}
cases_dict={}



# We will have to do this in two parts. One for the cases before the first FHM sheet (April 2) and one for the ones after

# THis is for the first part (extracted from FHM_lAtest
firstdate=sorted(df_date.keys())[0]
FirstDate=(datetime.strptime(firstdate,"%Y-%m-%d"))

firstpart={}
second={}
# Now we need to get cases per day etc..
for date in latest_df['Statistikdatum']:
    for region in latest_df.columns:
        key=region+date.strftime("%Y-%m-%d")
        if region=='Statistikdatum':
            continue
        elif region=='Totalt_antal_fall':
            if not key in second:
                second[key]={}
            second[key]['new_confirmed_cases']=latest_df.loc[latest_df['Statistikdatum']==date][region].max()
            second[key]['date']=date
            second[key]['country']=region
        if date<FirstDate:
            if not key in firstpart:
                firstpart[key]={}
            firstpart[key]['new_confirmed_cases']=latest_df.loc[latest_df['Statistikdatum']==date][region].max()
            firstpart[key]['date']=date
            firstpart[key]['country']=region
avlidna_df=df_map["Antal avlidna per dag"].fillna(0)
region="Totalt_antal_fall"
for date in avlidna_df['Datum_avliden']:
    if date=="Uppgift saknas": continue
    key=region+date.strftime("%Y-%m-%d")
    if date<FirstDate:
        if not key in firstpart:
            firstpart[key]={}
        firstpart[key]['new_deaths']=avlidna_df.loc[avlidna_df['Datum_avliden']==date]['Antal_avlidna'].max()
    #else:
    if not key in second:
        second[key]={}
    second[key]['new_deaths']=avlidna_df.loc[avlidna_df['Datum_avliden']==date]['Antal_avlidna'].max()

iva_df=df_map["Antal intensivvårdade per dag"].fillna(0)
region="Totalt_antal_fall"
for date in iva_df['Datum_vårdstart']:
    if date=="uppgift saknas": continue
    key=region+date.strftime("%Y-%m-%d")
    if date<FirstDate:
        if not key in firstpart:
            firstpart[key]={}
        firstpart[key]['new_iva']=iva_df.loc[iva_df['Datum_vårdstart']==date]['Antal_intensivvårdade'].max()
    #else:
    if not key in second:
        second[key]={}
    second[key]['new_iva']=iva_df.loc[iva_df['Datum_vårdstart']==date]['Antal_intensivvårdade'].max()

first_df=pd.DataFrame.from_dict(firstpart).transpose()    

fractiondeath={}
fractioniva={}
fractioncases={}
sumdeath=df_date[firstdate]["Totalt_antal_avlidna"].sum()
sumiva=df_date[firstdate]["Totalt_antal_intensivvårdade"].sum()
sumcases=df_date[firstdate]["Totalt_antal_fall"].sum()
for region in first_df["country"].drop_duplicates():
    #print (region,df_date[firstdate].loc[df_date[firstdate].Region==region]["Totalt_antal_avlidna"].max(),sumdeath)
    fractiondeath[region]=df_date[firstdate].loc[df_date[firstdate].Region==region]["Totalt_antal_avlidna"].max()/sumdeath
    fractioniva[region]=df_date[firstdate].loc[df_date[firstdate].Region==region]["Totalt_antal_intensivvårdade"].max()/sumiva
    fractioncases[region]=df_date[firstdate].loc[df_date[firstdate].Region==region]["Totalt_antal_fall"].max()/sumcases
    

    
    
#print(firstdate,fractiondeath,fractioniva,fractioncases)

#sys.exit()
#print(first_df)
#first_df.to_csv(reports_dir+"/first1.csv", sep=',')


length=len(first_df["country"])
first_df = first_df.assign(deaths=pd.Series(np.zeros(length)).values)
first_df = first_df.assign(iva=pd.Series(np.zeros(length)).values)
first_df = first_df.assign(confirmed=pd.Series(np.zeros(length)).values)

# We need a second compensation because death and iva patients are reported at a later stage
#ivafirstf=df_iva[firstdate]["Antal_intensivvårdade"].sum()
#deathfirst=df_iva[firstdate]["Antal_avlidna"].sum()



deathratio=df_date[firstdate]["Totalt_antal_avlidna"].sum()/first_df["new_deaths"].sum()
ivaratio=df_date[firstdate]["Totalt_antal_intensivvårdade"].sum()/first_df["new_iva"].sum()
#casereatio=df_date[firstdate]["Totalt_antal_fall"].sum()/first_df["confomenew_confirmed_cases"].sum()


#print (deathratio,deathratioB)
#print (ivaratio,ivaratioB)

#sys.exit()

sumdeath={}
sumiva={}
sumcases={}
for region in first_df["country"].drop_duplicates():
    sumdeath[region]=0
    sumiva[region]=0
    sumcases[region]=0
for date in sorted(first_df['date'].drop_duplicates()):
    #print (date)
    if date>=FirstDate: 
        continue # SHould not happe
    else:
        #temp_df=first_df.loc[first_df.date==date].fillna(0)
        ##print (temp_df,first_df)
        #totaldeath=temp_df.loc[temp_df.country=="Totalt_antal_fall"]["new_deaths"].max()
        #totaliva=temp_df.loc[temp_df.country=="Totalt_antal_fall"]["new_iva"].max()
        #totalcases=temp_df.loc[temp_df.country=="Totalt_antal_fall"]["new_confirmed_cases"].max()
        #print ("TEST",date,firstdate,date.strftime("%Y-%m-%d"))
        #Date=date.strftime("%Y-%m-%d")
        totaldeath=avlidna_df.loc[avlidna_df['Datum_avliden']==date]['Antal_avlidna'].max()
        totaliva=iva_df.loc[iva_df['Datum_vårdstart']==date]['Antal_intensivvårdade'].max()
        totalcases=latest_df.loc[latest_df['Statistikdatum']==date]["Totalt_antal_fall"].max()

        #print (totaldeath,totaliva,totalcases)
        #print (totaldeathB,totalivaB,totalcasesB)

        #if totaldeath != totaldeathB: sys.exit()
        
        for region in first_df["country"].drop_duplicates(): #temp_df["country"].drop_duplicates():
            #if region=="Totalt_antal_fall":continue
            d=totaldeath*fractiondeath[region]*deathratio
            i=totaliva*fractioniva[region]*ivaratio
            #c=totalcases*fractioncases[region]*caseratio
            c=first_df.loc[(first_df.date==date) & (first_df.country==region)]["new_confirmed_cases"].max()
            sumdeath[region]+=d
            sumiva[region]+=i
            sumcases[region]+=c
            first_df.loc[(first_df.date==date) & (first_df.country==region),["new_deaths"]]=d
            first_df.loc[(first_df.date==date) & (first_df.country==region),["new_iva"]]=i
            #first_df.loc[(first_df.date==date) & (first_df.country==region),["new_confirmed_cases"]]=c # This could be done 
            first_df.loc[(first_df.date==date) & (first_df.country==region),["deaths"]]=sumdeath[region] 
            first_df.loc[(first_df.date==date) & (first_df.country==region),["iva"]]=sumiva[region]
            first_df.loc[(first_df.date==date) & (first_df.country==region),["confirmed"]]=sumcases[region]




#first_df['cumdeath']
first_df=first_df.fillna(0)
#first_df.to_csv(reports_dir+"/first2.csv", sep=',')

#sys.exit()

#This is for econd part (extracted frem indiviudal dates)
#merged_df=pd.DataFrame(columns=['Date','country','confirmed','intensive','deaths','new_confirmed_cases'])


#second={}
first_df=first_df[first_df.country != "Totalt_antal_fall"]
#print (first_df)

for date in latest_df['Statistikdatum']:
    if date<FirstDate: continue
    for region in latest_df.columns:
        if region=='Statistikdatum': continue
        key=region+date.strftime("%Y-%m-%d")
        if not key in firstpart:
            second[key]={}
        second[key]['new_confirmed_cases']=latest_df.loc[latest_df['Statistikdatum']==date][region].max()
        second[key]['date']=date
        second[key]['country']=region

for date in sorted(df_date.keys()):
    df_date[date]=df_date[date].dropna()
    for region in df_date[date]['Region']:
        tmp_df=df_date[date].loc[df_date[date].Region==region]
        key=region+date
        if not key in second:
            second[key]={}
        #second[key]['date']=date
        #second[key]['country']=region
        second[key]['deaths']=tmp_df['Totalt_antal_avlidna'].max()
        second[key]['iva']=tmp_df['Totalt_antal_intensivvårdade'].max()
        second[key]['confirmed']=tmp_df['Totalt_antal_fall'].max()
    #tmp_df=df_date[date].loc[df_date[date].Region==region]
    region="Totalt_antal_fall"
    key=region+date
    if not key in second:
        second[key]={}
    #second[key]['date']=date
    #second[key]['country']=region
    second[key]['deaths']=df_date[date]['Totalt_antal_avlidna'].sum()
    second[key]['iva']=df_date[date]['Totalt_antal_intensivvårdade'].sum()
    second[key]['confirmed']=df_date[date]['Totalt_antal_fall'].sum()



second_df=pd.DataFrame.from_dict(second).transpose()
#second_df.to_csv(reports_dir+"/second1.csv", sep=',')
second_df=second_df.sort_values(by=['date'], na_position='first')


length=len(second_df["country"])
#second_df = second_df.assign(new_confirmed=pd.Series(np.zeros(sLength)).values)

cumdeath=0
cumiva=0
cumcases=0
second_df=second_df.fillna(0)
for date in sorted(second_df['date'].drop_duplicates()):
    #print (date)
    for region in second_df["country"].drop_duplicates(): #temp_df["country"].drop_duplicates():
        #print ("TEST",regopm,date)
        daybefore=date-timedelta(1)
        #print (date,daybefore,second_df.date,second_df.country)
        #print (type(date),type(daybefore))
        if date<FirstDate:
            if region=="Totalt_antal_fall":
                d=second_df.loc[(second_df.date==date) & (second_df.country==region)]["new_deaths"].max()
                i=second_df.loc[(second_df.date==date) & (second_df.country==region)]["new_iva"].max()
                c=second_df.loc[(second_df.date==date) & (second_df.country==region)]["new_confirmed_cases"].max()
                cumdeath+=float(d) 
                cumiva+=float(i)
                cumcases+=float(c)
                #print ("TEST",date,i,c,d,cumiva,cumdeath,cumcases)
                second_df.loc[(second_df.date==date) & (second_df.country==region),["deaths"]]=cumdeath
                second_df.loc[(second_df.date==date) & (second_df.country==region),["iva"]]=cumiva
                second_df.loc[(second_df.date==date) & (second_df.country==region),["confirmed"]]=cumcases
                #continue
        else:
            if date==FirstDate and region!="Totalt_antal_fall":


                before_df=first_df.loc[(first_df.date==daybefore) & (first_df.country==region)].fillna(0)
            else:
                before_df=second_df.loc[(second_df.date==daybefore) & (second_df.country==region)].fillna(0)
            thisday_df=second_df.loc[(second_df.date==date )& (second_df.country==region)].fillna(0)
            # Here we add values for the earlier
            #print ("Day:   ",region,date,second_df.loc[(second_df.date==date) & (second_df.country==region),["deaths"]].max())
            #print ("Before:",region,daybefore,second_df.loc[(second_df.date==daybefore) & (second_df.country==region),["deaths"]].max())
        
            d=thisday_df["deaths"].max()-before_df["deaths"].max()
            c=thisday_df["confirmed"].max()-before_df["confirmed"].max()
            i=thisday_df["iva"].max()-before_df["iva"].max()
            #print ("TEST",date,d,i,c)
        
            second_df.loc[(second_df.date==date) & (second_df.country==region),["new_deaths"]]=float(d)
            second_df.loc[(second_df.date==date) & (second_df.country==region),["new_iva"]]=float(i)
            #second_df.loc[(second_df.date==date) & (second_df.country==region),["new_confirmed_cases"]]=float(c)
            
        #second_df=second_df.fillna(0)

#df.set_value('C', 'x', 10)
#df.loc[df[<some_column_name>] == <condition>, [<another_column_name>]] = <value_to_add>

#print (second_df)
second_df=second_df.fillna(0)
#second_df.to_csv(reports_dir+"/second2.csv", sep=',')
merged_df=pd.concat([first_df, second_df], ignore_index=True)


#print (merged_df)
#merged_df.to_csv(reports_dir+"/merged1.csv", sep=',')


# We need to turn date to right type (datetime.

#merged_df.to_csv(reports_dir+"/merged4.csv", sep=',')
#try:
#agg_df['date']=agg_df.apply(lambda x:pp.FormatDate(x.DateRep), axis=1)

#print (agg_df)
    
#sys.exit()

# Remove a few dupliaed names
#merged_df=merged_df.drop_duplicates(['country','date'], keep='last').dropna()


# We shoudl complete the database with all missing dates.
first=merged_df["date"].to_list()[0]
firstdate=first
last=merged_df["date"].to_list()[-1]
lastdate=last



# We need to make two lists of countries
# Some parameters

countrylist={}
#sys.exit()
# This is all countries 
countries=merged_df['country'].drop_duplicates()
first={}
firstdate={}
startdate={}
firstdeaths={}
startdeaths={}
# Now we nedeathed to get the first date for each country (if <100 case last date)
for country in countries: # ["Afghanistan","Sweden","China"]: #countries:
    tempdf=merged_df.loc[merged_df['country'] == country]
    first[country]=tempdf["date"].to_list()[0]
    firstdate[country]=first[country]
    x=5
    try:
        start=tempdf[tempdf.confirmed > cf.minnum].iloc[0].date
    except:
        start=tempdf.date.tail(1).to_list()[0]
    try:
        deathsstart=tempdf[tempdf.deaths > cf.mindeaths].iloc[0].date
    except:
        deathsstart=tempdf.date.tail(1).to_list()[0]
    #if (type(start) is int):
    #    startdate[country]=parser.parse(start)
    #    startdeaths[country]=parser.parse(deathsstart)
    #else:
    startdate[country]=start
    startdeaths[country]=deathsstart

#print (startdeaths,startdate)
tiny=0.000001

def Days(x,y):
    return (x-startdate[y]).days
def DeathsDays(x,y):
    return (x-startdeaths[y]).days

merged_df['Days']=merged_df.apply(lambda x:Days(x.date,x.country), axis=1)
merged_df['DeathsDays']=merged_df.apply(lambda x:DeathsDays(x.date,x.country), axis=1)

#print (merged_df)

dates=merged_df.groupby(['date'])['date'].first().dropna()


merged_df['LogCases']=merged_df['confirmed'].apply(lambda x:(np.log2(max(x,tiny))))
merged_df['LogDeaths']=merged_df['deaths'].apply(lambda x:(np.log2(max(x,tiny))))
merged_df['Ratio'] = merged_df["deaths"]/(merged_df["confirmed"]+tiny)


linreg={}
countrylist={}
for country in countries:
    newdf=merged_df.loc[(merged_df['confirmed']>cf.minnum) & (merged_df['confirmed']<cf.maxnum) &(merged_df['country'] == country)]
    if (len(newdf)<4): # We need 6 points to fit a curve to the sigmoidal function.
        linreg[country]=linregress([0.0,1.0],[0.0,0.0])
        continue
    linreg[country]=linregress(newdf['Days'],newdf['LogCases'])
    countrylist[country]=linreg[country].slope

tmplist = sorted(countrylist.items() , reverse=True, key=lambda x: x[1])
sortedcountries=[]
for i in range(0,len(tmplist)):
    sortedcountries+=[tmplist[i][0]]

    # Sigmoidal (in log) funcion fit

#print (slopelist)

deathslist={}
deathsreg={}
for country in countries:
    newdf=merged_df.loc[(merged_df['deaths']>cf.mindeaths) & (merged_df['deaths']<cf.maxdeaths) & (merged_df['country'] == country)]
    if (len(newdf)<4):
        deathsreg[country]=linregress([0.0,1.0],[0.0,0.0])
        continue
    deathsreg[country]=linregress(newdf['DeathsDays'],newdf['LogDeaths'])
    #print(deathsreg[country])
    deathslist[country]=deathsreg[country].slope
tmplist = sorted(deathslist.items() , reverse=True, key=lambda x: x[1])
deathscountries=[]
for i in range(0,len(tmplist)):
    deathscountries+=[tmplist[i][0]]
    
    

def LinExp(x,y):
    return np.exp2(linreg[y].intercept+linreg[y].slope*x)

def DeathsExp(x,y):
    return np.exp2(deathsreg[y].intercept+deathsreg[y].slope*x)

if not os.path.exists(data_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + data_dir)

merged_df['LinCases']=merged_df.apply(lambda x:LinExp(x.Days,x.country), axis=1)
merged_df['LinDeaths']=merged_df.apply(lambda x:DeathsExp(x.DeathsDays,x.country), axis=1)
pp.fix_country_names(merged_df)

#optimized2_ydata = my_func(xdata, ydata, est2_w, est2_k)

merged_df=merged_df.sort_values(by=['country', 'date'])
merged_df.to_csv(reports_dir+"/"+datafile, sep=',')

# One version suitable for saln
slan_df=merged_df[["date","new_confirmed_cases","new_deaths","country"]]
slan_df.to_csv(reports_dir+"/FHM-"+today.strftime("%Y-%m-%d")+".csv", sep=',')
