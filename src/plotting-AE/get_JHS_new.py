#!/usr/bin/env python3
from __future__ import print_function
import pandas as pd
import numpy as np
import os
import sys, traceback
from pathlib import Path
import argparse
from argparse import RawTextHelpFormatter
import re
import math
import wget
import docopt
#import pickle
import os.path
import operator
from dominate import document
from dominate.tags import *

from dateutil.parser import parse
from datetime import datetime,date,time, timedelta
from dateutil import parser
#import pyarrow
#import matplotlib.pyplot as plt
#import matplotlib as mpl
# %matplotlib inline
from scipy.stats import linregress
from scipy.optimize import curve_fit

#mpl.rc('figure', max_open_warning = 0)

import preprocess as pp
import config as cf


##args = docopt.docopt(__doc__)
#out_dir = args['--output_folder']


#first we run data_pryp.py from covodify (slightly modifcied)


p = argparse.ArgumentParser(description =  '- get_JHS.py - Extract data from JHS and format it to merged.csv -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-f','--force', required= False, help='Force', action='store_true')
p.add_argument('-i','--input', required= False, help='Force')
p.add_argument('-p','--province', required= False, help='Force', action='store_true')
p.add_argument('-out','--output_folder', required= False, help='output folder')
ns = p.parse_args()

if ns.province:
    datafile="merged_province_JHS.csv"
else:
    datafile="merged_JHS.csv"

if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=home = str(Path.home())+"/Desktop/Corona/"



    
# Dynamic parameters
data_dir  = os.path.join(out_dir,'data') 
image_dir =  out_dir 
nation_dir =  os.path.join(out_dir,'nations')
reports_dir = os.path.join(out_dir,'data')

if not os.path.exists(image_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + image_dir)
if not os.path.exists(nation_dir):
    print('Creating nation folder...')
    os.system('mkdir -p ' + nation_dir)
if not os.path.exists(data_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + data_dir)
if not os.path.exists(reports_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + reports_dir)

today=date.today()
yesterday=date.today() - timedelta(1)
aweekago=date.today() - timedelta(7)




# Get new data
#os.system("python3 data_prep.py")

if not ns.input:
    ns.input=str(Path.home())+"/Documents/COVID-19/csse_covid_19_data/csse_covid_19_time_series/"



globalsets={"Global-confirmed":"time_series_covid19_confirmed_global.csv",
          "Global-deaths":"time_series_covid19_deaths_global.csv",
          "Global-recovered":"time_series_covid19_recovered_global.csv"}
USsets={"US-confirmed":"time_series_covid19_confirmed_US.csv",
        "US-deaths":"time_series_covid19_deaths_US.csv"}
datasets={"Global-confirmed":"time_series_covid19_confirmed_global.csv",
          "Global-deaths":"time_series_covid19_deaths_global.csv",
          "Global-recovered":"time_series_covid19_recovered_global.csv",
          "US-confirmed":"time_series_covid19_confirmed_US.csv",
          "US-deaths":"time_series_covid19_deaths_US.csv"}

names={"Global-confirmed":"confirmed",
          "Global-deaths":"deaths",
          "Global-recovered":"recovered",
          "US-confirmed":"confirmed",
          "US-deaths":"deaths"}

    
print('Importing Data...')
print("Using: ",ns.input)
df_date={}

outfile=data_dir+"/"+datafile


#print(infile)
if  os.path.isfile(outfile) and (not ns.force):
    df=pd.read_csv(outfile)
    df['date']=df.apply(lambda x:pp.FormatDateMerged(x.date), axis=1)
    #print (df.loc[df.date==today],len(df.loc[df.date==today]))
    if len(df.loc[df.date==today])>0 and (not ns.force):
        print ("There is no new data yet")
        print(", use --force to rerun on yesterdays data")
        sys.exit(0)
    

# First we test if we already run the data for today:

    
csvfile={}
data_df={}
for set in (datasets.keys()):
    #print (URL+datasets[set], data_dir+"/"+datasets[set])
    #csvfile[set]=wget.download(URL+datasets[set], out=str(data_dir+"/"+datasets[set]))
    data_df[set]=pd.read_csv(ns.input+"/"+datasets[set])
    #print (set,data_df)
    
merged_df = pd.DataFrame([])



#Province/State,Country/Region,Lat,Long
for set in (globalsets.keys()):
    dates=data_df[set].columns[4:]
    #print (dates[4:])
    #data_df[set].rename(columns={
    #    'Province/State': 'province',
    #    'Country/Region,': 'country'
    #})
    del data_df[set]["Lat"]
    del data_df[set]["Long"]
    #print (set,data_df[set])
    if ns.province:
        data_df[set]=data_df[set].dropna()
        data_df[set]["Province/State"]=data_df[set]["Province/State"].fillna('')
        data_df[set]["country"] = data_df[set][["Country/Region","Province/State"]].agg('-'.join, axis=1)
        del data_df[set]["Country/Region"]
        del data_df[set]["Province/State"]
    else:
        data_df[set]=data_df[set].groupby(['Country/Region'])[dates].sum().reset_index()
        data_df[set]=data_df[set].rename(columns={'Country/Region': 'country'})
        #data_df[set].columns=[dates+ ['country']]
        #del data_df[set]["Country/Region"]
    #print(data_df[set])
    #data_df[set+"-dates"]=data_df[set][dates]
    #print(data_df[set])
    #print(data_df[set+"-dates"])
    countries=data_df[set].country.drop_duplicates()


for country in countries:
    temp_df=pd.DataFrame(["country","date"])
    newdf={}
    for set in globalsets.keys():
        newdf[set]=data_df[set].loc[data_df[set].country==country].transpose().reset_index()    
        newdf[set].columns = ['date',names[set]]
        newdf[set]["country"]=country
        #print (newdf[set])
        newdf[set]=newdf[set].loc[newdf[set].date!="country"]
        #newdf['date']=df.apply(lambda x:pp.FormatDate(x.date), axis=1)
        #print (newdf[set])
        #sys.exit()
        
    temp_df=pd.merge(newdf["Global-confirmed"],newdf["Global-deaths"], how='left', on=['country','date'])
    temp_df=pd.merge(temp_df,newdf["Global-recovered"], how='left', on=['country','date'])
    temp_df['new_confirmed_cases'] = temp_df['confirmed'].diff().fillna(0)
    temp_df['new_deaths'] = temp_df['deaths'].diff().fillna(0)
    temp_df['new_recovered'] = temp_df['recovered'].diff().fillna(0)


    merged_df=merged_df.append(temp_df)

#print (merged_df)
#sys.exit()
#UID,iso2,iso3,code3,FIPS,Admin2,Province_State,Country_Region,Lat,Long_,Combined_Key,Population

# We need to group US by states




popdata=data_df["US-deaths"][["Combined_Key","Population"]]
del data_df["US-deaths"]["Population"]

#        dates=data_df[set].columns[12:]

if (ns.province):
    for set in (USsets.keys()):
        data_df[set]=data_df[set].groupby(['Province_State','Country_Region'])[dates].sum().reset_index()
        #sys.exit()
        #print (dates[4:])
        data_df[set]["country"] = data_df[set][["Country_Region","Province_State"]].agg('-'.join, axis=1)
        #data_df[set].rename(columns={
        #    'Province/State': 'province',
        #    'Country/Region,': 'country'
        #})
        #for key in ["UID","iso2","iso3","code3","FIPS","Admin2","Province_State","Country_Region","Lat","Long_"]:
        for key in ["Province_State","Country_Region"]:
            del data_df[set][key]
        #print (set,data_df[set])
        data_df[set]=data_df[set].rename(columns={'Combined_Key': 'country'})
        #print(data_df[set])
        #data_df[set+"-dates"]=data_df[set][dates]
        #print(data_df[set])
        #print(data_df[set+"-dates"])
        countries=data_df[set].country.drop_duplicates()


    for country in countries:
        newdf={}
        for set in USsets.keys():
            newdf[set]=data_df[set].loc[data_df[set].country==country].transpose().reset_index()
            newdf[set].columns = ['date',names[set]]
            newdf[set]["country"]=country
            newdf[set]=newdf[set].loc[newdf[set].date!="country"]
            #newdf['date']=df.apply(lambda x:pp.FormatDate(x.date), axis=1)
            #print (newdf[set])

            
        temp_df=pd.merge(newdf["US-confirmed"],newdf["US-deaths"], how='left', on=['country','date'])
        temp_df['new_confirmed_cases'] = temp_df['confirmed'].diff().fillna(0)
        temp_df['new_deaths'] = temp_df['deaths'].diff().fillna(0)


        merged_df=merged_df.append(temp_df)


pp.fix_country_names(df)

pp.fix_states(merged_df)
merged_df['date']=merged_df.apply(lambda x:pp.FormatUSDate(x.date), axis=1)
print (merged_df)



first=merged_df["date"].to_list()[0]
firstdate=first
last=merged_df["date"].to_list()[-1]
lastdate=last



# We need to make two lists of countries
# Some parameters

countrylist={}
#sys.exit()
# This is all countries 
countries=merged_df['country'].drop_duplicates()
first={}
firstdate={}
startdate={}
firstdeaths={}
startdeaths={}
# Now we nedeathed to get the first date for each country (if <100 case last date)
for country in countries: # ["Afghanistan","Sweden","China"]: #countries:
    tempdf=merged_df.loc[merged_df['country'] == country]
    first[country]=tempdf["date"].to_list()[0]
    firstdate[country]=first[country]
    x=5
    try:
        start=tempdf[tempdf.confirmed > cf.minnum].iloc[0].date
    except:
        start=tempdf.date.tail(1).to_list()[0]
    try:
        deathsstart=tempdf[tempdf.deaths > cf.mindeaths].iloc[0].date
    except:
        deathsstart=tempdf.date.tail(1).to_list()[0]
    #if (type(start) is int):
    #    startdate[country]=parser.parse(start)
    #    startdeaths[country]=parser.parse(deathsstart)
    #else:
    startdate[country]=start
    startdeaths[country]=deathsstart

#print (startdeaths,startdate)
#tiny=0.000001

def Days(x,y):
    return (x-startdate[y]).days
def DeathsDays(x,y):
    return (x-startdeaths[y]).days

merged_df['Days']=merged_df.apply(lambda x:Days(x.date,x.country), axis=1)
merged_df['DeathsDays']=merged_df.apply(lambda x:DeathsDays(x.date,x.country), axis=1)

#print (merged_df)

dates=merged_df.groupby(['date'])['date'].first().dropna()

#merged_df.to_csv(reports_dir+"/merged1.csv", sep=',')
merged_df['LogCases']=merged_df['confirmed'].apply(lambda x:(np.log2(max(x,cf.tiny))))
merged_df['LogDeaths']=merged_df['deaths'].apply(lambda x:(np.log2(max(x,cf.tiny))))
merged_df['Ratio'] = merged_df["deaths"]/merged_df["confirmed"]


linreg={}
countrylist={}
for country in countries:
    newdf=merged_df.loc[(merged_df['confirmed']>cf.minnum) & (merged_df['confirmed']<cf.maxnum) &(merged_df['country'] == country)]
    if (len(newdf)<4): # We need 6 points to fit a curve to the sigmoidal function.
        linreg[country]=linregress([0.0,1.0],[0.0,0.0])
        continue
    linreg[country]=linregress(newdf['Days'],newdf['LogCases'])
    countrylist[country]=linreg[country].slope

tmplist = sorted(countrylist.items() , reverse=True, key=lambda x: x[1])
sortedcountries=[]
for i in range(0,len(tmplist)):
    sortedcountries+=[tmplist[i][0]]

    # Sigmoidal (in log) funcion fit

#print (slopelist)

deathslist={}
deathsreg={}
for country in countries:
    newdf=merged_df.loc[(merged_df['deaths']>cf.mindeaths) & (merged_df['deaths']<cf.maxdeaths) & (merged_df['country'] == country)]
    if (len(newdf)<4):
        deathsreg[country]=linregress([0.0,1.0],[0.0,0.0])
        continue
    deathsreg[country]=linregress(newdf['DeathsDays'],newdf['LogDeaths'])
    #print(deathsreg[country])
    deathslist[country]=deathsreg[country].slope
tmplist = sorted(deathslist.items() , reverse=True, key=lambda x: x[1])
deathscountries=[]
for i in range(0,len(tmplist)):
    deathscountries+=[tmplist[i][0]]
    
    

def LinExp(x,y):
    return np.exp2(linreg[y].intercept+linreg[y].slope*x)

def DeathsExp(x,y):
    return np.exp2(deathsreg[y].intercept+deathsreg[y].slope*x)


merged_df['LinCases']=merged_df.apply(lambda x:LinExp(x.Days,x.country), axis=1)
merged_df['LinDeaths']=merged_df.apply(lambda x:DeathsExp(x.DeathsDays,x.country), axis=1)

pp.fix_country_names(merged_df)

#optimized2_ydata = my_func(xdata, ydata, est2_w, est2_k)

merged_df=merged_df.sort_values(by=['country', 'date'])
merged_df.to_csv(outfile)
