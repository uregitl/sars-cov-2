import numpy as np
#from dateutil.parser import parse
from datetime import datetime # ,date,time, timedelta
#from dateutil import parser
import csv
import os
import numpy as np
import pandas as pd
import math
from datetime import datetime,date,time, timedelta

translations = {' Azerbaijan':'Azerbaijan',
                'America-American Samoa':'US-American_Samoa',
                'America-Cruise Ship Princess':'Others',
                'America-Cruise Ship Princess)':'Others',
                'America-District of Columbia':'US-District_of_Columbia',
                'America-Grand Princess':'Others',
                'America-New Hampshire':'US-New_Hampshire',
                'America-New Jersey':'US-New_Jersey',
                'America-New Mexico':'US-New_Mexico',
                'America-New York':'US-New_York',
                'America-North Carolina':'US-North_Carolina',
                'America-North Dakota':'US-North_Dakota',
                'America-Northern Mariana Islands':'US-Northern_Mariana_Islands',
                'America-Oregon ':'US-Oregon',
                'America-Puerto Rico':'US-Puerto_Rico',
                'America-Rhode Island':'US-Rhode_Island',
                'America-South Carolina':'US-South_Carolina',
                'America-South Dakota':'US-South_Dakota',
                'America-United States Virgin Islands':'US-Virgin_Islands',
                'America-Virgin Islands':'US-Virgin_Islands',
                'America-Washington, D.C.':'US-Washington,_D.C.',
                'America-West Virginia':'US-West_Virginia',
                'America-Wuhan Evacuee':'US-Wuhan_Evacuee',
                'Antigua and Barbuda-Antigua and Barbuda':'Antigua_and_Barbuda',
                'Antigua and Barbuda':'Antigua_and_Barbuda',
                'Aosta':"Valle D'Aosta/Vallée D'Aoste",
                'Apulia':'Puglia',
                'Australia-Australian Capital Territory':'Australia-Australian_Capital_Territory',
                'Australia-Cruise Ship Princess':'Others',
                'Australia-External territories':'Australia-External_territories',
                'Australia-Jervis Bay Territory':'Australia-Jervis_Bay_Territory',
                'Australia-New South Wales':'Australia-New_South_Wales',
                'Australia-Northern Territory':'Australia-Northern_Territory',
                'Australia-South Australia':'Australia-South_Australia',
                'Australia-Western Australia':'Australia-Western_Australia',
                'Bahamas, The':'Bahamas',
                'Bahamas':'Bahamas',
                'Balearic Islands':'Baleares',
                'Basque Country':'Pais Vasco',
                'Blekinge County':'Blekinge',
                'Bosnia and Herzegovina':'Bosnia_and_Herzegovina',
                'Bosnia':'Bosnia_and_Herzegovina',
                'Burkina Faso-Burkina Faso':'Burkina_Faso',
                'Burkina Faso':'Burkina_Faso',
                'Cabo Verde-Cabo Verde':'Cabo_Verde',
                'Cabo Verde':'Cape_Verde',
                'Canada- Montreal, QC':'Canada-_Montreal_QC',
                'Canada-British Columbia':'Canada-British_Columbia',
                'Canada-Calgary, Alberta':'Canada-Calgary_Alberta',
                'Canada-Cruise Ship Princess':'Others',
                'Canada-Edmonton, Alberta':'Canada-Edmonton_Alberta',
                'Canada-Grand Princess':'Others',
                'Canada-London, ON':'Canada-London_ON',
                'Canada-New Brunswick':'Canada-New_Brunswick',
                'Canada-Newfoundland and Labrador':'Canada-Newfoundland_and_Labrador',
                'Canada-Northwest Territories':'Canada-Northwest_Territories',
                'Canada-Nova Scotia':'Canada-Nova_Scotia',
                'Canada-Prince Edward Island':'Canada-Prince_Edward_Island',
                'Canada-Toronto, ON':'Canada-Toronto_ON',
                'Canary Islands':'Canarias',
                'Cape Verde-Cape Verde':'Cape_Verde',
                'Cape Verde':'Cape_Verde',
                'Cases_on_an_international_conveyance_Japan':'Others',
                'Castile and León':'Castilla-Leon',
                'Castile-La Mancha':'Castilla-La Mancha',
                'Catalonia':'Cataluña',
                'Cayman Islands-Cayman Islands':'Cayman_Islands',
                'Cayman Islands':'Cayman_Islands',
                'Central African Republic-Central African Republic':'Central_African_Republic',
                'Central African Republic':'Central_African_Republic',
                'Central_African_Republic':'CAR',
                'Channel Islands-Channel Islands':'Channel_Islands',
                'Channel Islands':'Channel_Islands',
                'China-Hong Kong':'China-Hong_Kong',
                'China-Inner Mongolia':'China-Inner_Mongolia',
                'Community of Madrid':'Madrid',
                'Congo (Brazzaville:)':'Congo',
                'Congo (Kinshasa)':'Congo',
                'Congo-Congo (Brazzaville)':'Congo',
                'Costa Rica-Costa Rica':'Costa_Rica',
                'Costa Rica':'Costa_Rica',
                'Cote d\'Ivoire-Cote d\'Ivoire':'Cote_dIvoire',
                'Cote d\'Ivoire':'Cote_dIvoire',
                'Cruise Ship Princess-Cruise Ship Princess':'Others',
                'Cruise Ship Princess':'Others',
                'Cruise Ship':'Others',
                'Czech Republic':'Czechia',
                'Dalarna County':'Dalarna',
                'Democratic_Republic_of_the_Congo':'Congo',
                'Denmark-Faroe Islands':'Denmark-Faroe_Islands',
                'Dominican Republic-Dominican Republic':'Dominican_Republic',
                'Dominican Republic':'Dominican_Republic',
                'East Timor-East Timor':'East_Timor',
                'East Timor':'East_Timor',
                'El Salvador-El Salvador':'El_Salvador',
                'El Salvador':'El_Salvador',
                'Equatorial Guinea-Equatorial Guinea':'Equatorial_Guinea',
                'Equatorial Guinea':'Equatorial_Guinea',
                'Faroe Islands-Faroe Islands':'Faroe_Islands',
                'Faroe Islands':'Faroe_Islands',
                'France-Fench Guiana':'France-French_Guiana',
                'France-French Guiana':'France-French_Guiana',
                'France-French Polynesia':'France-French_Polynesia',
                'France-New Caledonia':'France-New_Caledonia',
                'France-Saint Barthelemy':'France-Saint_Barthelemy',
                'France-Saint Pierre and Miquelon':'France-Saint_Pierre_and_Miquelon',
                'France-St Martin':'France-St_Martin',
                'French Guiana-French Guiana':'France-French_Guiana-',
                'French Guiana':'French_Guiana',
                'Friuli Venezia Giulia':'Friuli_Venezia_Giulia',
                'Friuli-Venezia Giulia':'Friuli Venezia Giulia',
                'Gambia, The':'Gambia',
                'Gavleborg County':'Gävleborg',
                'Gotland County':'Gotland',
                'Halland County':'Halland',
                'Holy See-Holy See':'Holy_See',
                'Holy See':'Holy_See',
                'Hong Kong SAR':'Hong Kong',
                'Hong Kong-Hong Kong':'China-Hong_Kong',
                'Hong Kong':'Hong_Kong',
                'Iran (Islamic Republic of)':'Iran, Islamic Republic of',
                'Israel-Cruise Ship Princess':'Others',
                'Ivory Coast-Ivory Coast':'Ivory_Coast',
                'Ivory Coast':'Ivory_Coast',
                'Jamtland County':'Jämtland_Härjedalen',
                'Jämtland Härjedalen':'Jämtland_Härjedalen',
                'Jonkoping County':'Jönköping',
                'Kalmar County':'Kalmar',
                'Korea, South':'South Korea',
                'Kronoberg County':'Kronoberg',
                'Lombardy':'Lombardia',
                'Macao SAR':'Macau',
                'Mainland China':'China',
                'Mississippi Zaandam-Mississippi Zaandam':'Mississippi_Zaandam',
                'Mississippi Zaandam':'Mississippi_Zaandam',
                'Myanmar (Burma)':'Myanmar',
                'Navarre':'Navarra',
                'Netherlands-Bonaire, Sint Eustatius and Saba':'Netherlands-Bonaire_Sint_Eustatius_and_Saba',
                'Netherlands-Sint Maarten':'Netherlands-Sint_Maarten',
                'New Zealand-New Zealand':'New_Zealand-New_Zealand',
                'New Zealand':'New_Zealand',
                'Norrbotten County':'Norrbotten',
                'North Ireland':'Ireland',
                'North Macedonia-North Macedonia':'North_Macedonia-North_Macedonia',
                'North Macedonia':'North_Macedonia',
                'occupied Palestinian territory-occupied Palestinian territory':'Palestine',
                'occupied Palestinian territory':'Palestine',
                'Örebro County':'Örebro',
                'Östergötland County':'Östergötland',
                'Others-Cruise Ship Princess cruise ship':'Others',
                'Others-Cruise Ship Princess':'Others',
                'Others-Cruise Ship':'Others',
                'P.A. Bolzano':'P.A._Bolzano',
                'P.A. Trento':'Trentino-Alto_Adige_Sudtirol',
                'Papua New Guinea-Papua New Guinea':'Papua_New_Guinea',
                'Papua New Guinea':'Papua_New_Guinea',
                'Piedmont':'Piemonte',
                'Puerto Rico-Puerto Rico':'Puerto_Rico',
                'Puerto Rico':'Puerto_Rico',
                'Region of Murcia':'Murcia',
                'Republic of Ireland':'Ireland',
                'Republic of Korea':'South Korea',
                'Republic of Moldova':'Moldova',
                'Republic of the Congo':'Congo',
                'Russian Federation-Russian Federation':'Russia',
                'Russian Federation':'Russian_Federation',
                'Saint Barthelemy-Saint Barthelemy':'Saint_Barthelemy',
                'Saint Barthelemy':'Saint_Barthelemy',
                'Saint Kitts and Nevis-Saint Kitts and Nevis':'Saint_Kitts_and_Nevis',
                'Saint Kitts and Nevis':'Saint_Kitts_and_Nevis',
                'Saint Lucia-Saint Lucia':'Saint_Lucia',
                'Saint Lucia':'Saint_Lucia',
                'Saint Martin-Saint Martin':'Saint_Martin',
                'Saint Martin':'Saint_Martin',
                'Saint Vincent and the Grenadines-Saint Vincent and the Grenadines':'Saint_Vincent_and_the_Grenadines',
                'Saint Vincent and the Grenadines':'Saint_Vincent_and_the_Grenadines',
                'San Marino-San Marino':'San_Marino',
                'San Marino':'San_Marino',
                'Sao Tome and Principe-Sao Tome and Principe':'Sao_Tome_and_Principe',
                'Sao Tome and Principe':'Sao_Tome_and_Principe',
                'Sardinia':'Sardegna',
                'Saudi Arabia-Saudi Arabia':'Saudi_Arabia',
                'Saudi Arabia':'Saudi_Arabia',
                'Sierra Leone-Sierra Leone':'Sierra_Leone',
                'Sierra Leone':'Sierra_Leone',
                'Skåne County':'Skåne',
                'Södermanland County':'Sörmland',
                'South Africa-South Africa':'South_Africa',
                'South Africa':'South_Africa',
                'South Korea-South Korea':'South_Korea',
                'South Korea':'South_Korea',
                'South Sudan-South Sudan':'South_Sudan',
                'South Sudan':'South_Sudan',
                'Sri Lanka-Sri Lanka':'Sri_Lanka',
                'Sri Lanka':'Sri_Lanka',
                'St. Martin-St. Martin':'St_Martin._Martin',
                'St. Martin':'St._Martin',
                'Stockholm County':'Stockholm',
                'Taipei and environs':'Taiwan',
                'Taiwan*':'Taiwan',
                'The Bahamas-The Bahamas':'Bahamas',
                'The Bahamas':'Bahamas',
                'The Gambia':'Gambia',
                'Trentino-Alto Adige/Sudtirol':'Trentino-Alto_Adige_Sudtirol',
                'Trentino-Alto:Adige/Sudtirol':'Trentino-Alto_Adige_Sudtirol',
                'Trentino-South Tyrol':'Trentino-Alto_Adige_Sudtirol',
                'Trinidad and Tobago-Trinidad and Tobago':'Trinidad_and_Tobago',
                'Trinidad and Tobago':'Trinidad_and_Tobago',
                'Tuscany':'Toscana',
                'UK-Anguilla':'United_Kingdom-Anguilla',
                'UK-Bermuda':'United_Kingdom-Bermuda',
                'UK-British Virgin Islands':'United_Kingdom-British_Virgin_Islands',
                'UK-Cayman Islands':'United_Kingdom-Cayman_Islands',
                'UK-Channel Islands':'United_Kingdom-Channel_Islands',
                'UK-Falkland Islands (Islas Malvinas)':'United_Kingdom-Falkland_Islands',
                'UK-Falkland Islands (Malvinas)':'United_Kingdom-Falkland_Islands',
                'UK-Gibraltar':'United_Kingdom-Gibraltar',
                'UK-Isle of Man':'United_Kingdom-Isle_of_Man',
                'UK-Montserrat':'United_Kingdom-Montserrat',
                'UK-Turks and Caicos Islands':'United_Kingdom-Turks_and_Caicos_Islands',
                'UK-UK':'UK',
                'UK-United Kingdom':'UK',
                'United Arab Emirates-United Arab Emirates':'United_Arab_Emirates',
                #'United Arab Emirates':'UAE',
                'United Arab Emirates':'United_Arab_Emirates',
                'United Kindom':'UK',
                'United Kingdom':'UK',
                'United_Kingdom':'UK',
                'United_Republic_of_Tanzania':'Tanzania',
                'United_States_of_America':'USA',
                'Unites States':'USA',
                'Uppsala County':'Uppsala',
                'US-Grand Princess Cruise Ship':'Others',
                'US':'USA',
                'Valencian Community':'Valencia',
                "Valle d'Aosta":'Valle_d_Aosta',
                "Valle D'Aosta/Vallée D'Aoste'":'Valle_D_Aosta_Vallee_D_Aoste',
                'Valle_D_Aosta/Vallee_D_Aoste':'Valle_D_Aosta_Vallee_D_Aoste',
                'Varmland County':'Värmland',
                'Västerbotten County':'Västerbotten',
                'Västernorrland County':'Västernorrland',
                'Västmanland County':'Västmanland',
                'Västra Götaland County':'Västra_Götaland',
                'Västra Götaland':'Västra_Götaland',
                'Vatican City-Vatican City':'Vatican_City-Vatican_City',
                'Vatican City':'Vatican_City',
                'Viet Nam':'Vietnam',
                'West Bank and Gaza-West Bank and Gaza':'West_Bank_and_Gaza',
                'West Bank and Gaza':'West_Bank_and_Gaza',
                'Western Sahara-Western Sahara':'Western_Sahara',
                'Western Sahara':'Western_Sahara',
                }
def fix_country_names(df):
    df.replace(translations, inplace=True)


def fix_states(df):
    df.replace(['.*AL'],['Alabama'],regex=True,inplace=True)
    df.replace(['.*AK'],['Alaska'],regex=True,inplace=True)
    df.replace(['.*AZ'],['Arizona'],regex=True,inplace=True)
    df.replace(['.*AR'],['Arkansas'],regex=True,inplace=True)
    df.replace(['.*CA'],['California'],regex=True,inplace=True)
    df.replace(['.*CO'],['Colorado'],regex=True,inplace=True)
    df.replace(['.*CT'],['Connecticut'],regex=True,inplace=True)
    df.replace(['.*DE'],['Delaware'],regex=True,inplace=True)
    df.replace(['.*DC'],['District of Columbia'],regex=True,inplace=True)
    df.replace(['.*FL'],['Florida'],regex=True,inplace=True)
    df.replace(['.*GA'],['Georgia'],regex=True,inplace=True)
    df.replace(['.*HI'],['Hawaii'],regex=True,inplace=True)
    df.replace(['.*ID'],['Idaho'],regex=True,inplace=True)
    df.replace(['.*IA'],['Iowa'],regex=True,inplace=True)
    df.replace(['.*IL'],['Illinois'],regex=True,inplace=True)
    df.replace(['.*IN'],['Indiana'],regex=True,inplace=True)
    df.replace(['.*KS'],['Kansas'],regex=True,inplace=True)
    df.replace(['.*KY'],['Kentucky'],regex=True,inplace=True)
    df.replace(['.*LA'],['Louisiana'],regex=True,inplace=True)
    df.replace(['.*ME'],['Maine'],regex=True,inplace=True)
    df.replace(['.*MD'],['Maryland'],regex=True,inplace=True)
    df.replace(['.*MA'],['Massachusetts'],regex=True,inplace=True)
    df.replace(['.*MI'],['Michigan'],regex=True,inplace=True)
    df.replace(['.*MN'],['Minnesota'],regex=True,inplace=True)
    df.replace(['.*MS'],['Mississippi'],regex=True,inplace=True)
    df.replace(['.*MO'],['Missouri'],regex=True,inplace=True)
    df.replace(['.*MT'],['Montana'],regex=True,inplace=True)
    df.replace(['.*NE'],['Nebraska'],regex=True,inplace=True)
    df.replace(['.*NV'],['Nevada'],regex=True,inplace=True)
    df.replace(['.*NH'],['New Hampshire'],regex=True,inplace=True)	
    df.replace(['.*NJ'],['New Jersey'],regex=True,inplace=True)	
    df.replace(['.*NM'],['New Mexico'],regex=True,inplace=True)	
    df.replace(['.*NY'],['New York'],regex=True,inplace=True)	
    df.replace(['.*NC'],['North Carolina'],regex=True,inplace=True)	
    df.replace(['.*ND'],['North Dakota'],regex=True,inplace=True)	
    df.replace(['.*OH'],['Ohio'],regex=True,inplace=True)
    df.replace(['.*OK'],['Oklahoma'],regex=True,inplace=True)
    df.replace(['.*OR'],['Oregon'],regex=True,inplace=True)
    df.replace(['.*PA'],['Pennsylvania'],regex=True,inplace=True)
    df.replace(['.*RI'],['Rhode Island'],regex=True,inplace=True)	
    df.replace(['.*SC'],['South Carolina'],regex=True,inplace=True)	
    df.replace(['.*SD'],['South Dakota'],regex=True,inplace=True)	
    df.replace(['.*TN'],['Tennessee'],regex=True,inplace=True)
    df.replace(['.*TX'],['Texas'],regex=True,inplace=True)
    df.replace(['.*UT'],['Utah'],regex=True,inplace=True)
    df.replace(['.*VT'],['Vermont'],regex=True,inplace=True)
    df.replace(['.*VA'],['Virginia'],regex=True,inplace=True)
    df.replace(['.*WA'],['Washington'],regex=True,inplace=True)
    df.replace(['.*WV'],['West Virginia'],regex=True,inplace=True)
    df.replace(['.*WI'],['Wisconsin'],regex=True,inplace=True)
    df.replace(['.*WY'],['Wyoming'],regex=True,inplace=True)
    df.replace(['.*WY'],['Wyoming'], regex=True, inplace=True)
    df.replace(['.*Diamond*'],['Others'], regex=True, inplace=True)
    df.replace(['Virgin Islands, U.S.'],['Virgin Islands'], regex=True, inplace=True)
    
def sigmoidalfunction(x,y,k,m,a,b):
    f=[]
    for i in x:
        f+=[(m-b) / (1 + np.exp(-k*(i-a))) + b ]
    return f
def sigmoidalfunction0(x,y,k,m,a):
    f=[]
    b=0
    for i in x:
        f+=[(m-b) / (1 + np.exp(-k*(i-a))) + b ]
    return f

def FormatDate(x):
    #return (parser.parse(x))
    return (datetime.strptime(x,"%d/%m/%Y"))

def FormatUSDate(x):
    #return (parser.parse(x))
    return (datetime.strptime(x,"%m/%d/%y"))

def FormatDateMerged(x):
    #return (parser.parse(x))
    return (datetime.strptime(x,"%Y-%m-%d"))

def replace_arg_space(country_str):
    return country_str.replace(' ', '_')

def replace_arg_score(country_str):
    return country_str.replace('_', ' ')
def map_names(df):
    translations = {'Antigua_and_Barbuda':'Antigua and Barbuda',
                    'Bosnia_and_Herzegovina':'Bosnia and Herzegovina',
                    'British_Virgin_Islands':'British Virgin Islands',
                    'Brunei_Darussalam':'Brunei Darussalam',
                    'Burkina_Faso':'Burkina Faso',
                    'CAR':'Central African Republic',
                    'Central_African_Republic':'Central African Republic',
                    'Cape_Verde':'Cape Verde',
                    'Cayman_Islands':'Cayman Islands',
                    'Costa_Rica':'Costa Rica',
                    'Cote_dIvoire':"Côte d'Ivoire",
                    'Czechia':'Czech Republic',
                    'Dominican_Republic':'Dominican Republic',
                    'El_Salvador':'El Salvador',
                    'Vietnam':'Viet Nam',
                    'Equatorial_Guinea':'Equatorial Guinea',
                    'Faroe_Islands':'Faroe Islands',
                    'French_Polynesia':'French Polynesia',
                    'Guinea_Bissau':'Guinea-Bissau',
                    'Holy_See':'Holy See (Vatican City State)',
                    'Isle_of_Man':'Isle of Man',
                    'Ivore Cost':'Ivore Coast',
                    'New_Caledonia':'New Caledonia',
                    'New_Zealand':'New Zealand',
                    'North_Macedonia':'Macedonia, the former Yugoslav Republic of',
                    'Northern_Mariana_Islands':'Northern Mariana Islands',
                    'Papua_New_Guinea':'Papua New Guinea',
                    'Puerto_Rico':'Puerto Rico',
                    'Saint_Kitts_and_Nevis':'Saint Kitts and Nevis',
                    'Saint_Lucia':'Saint Lucia',
                    'Sint_Maarten':'Sint Maarten (Dutch part)',
                    'Saint_Vincent_and_the_Grenadines':'Saint Vincent and the Grenadines',
                    'San_Marino':'San Marino',
                    'Sao_Tome_and_Principe':'Sao Tome and Principe',
                    'Saudi_Arabia':'Saudi Arabia',
                    'Sierra_Leone':'Sierra Leone',
                    'South_Africa':'South Africa',
                    'South_Korea':'Korea, Republic of',
                    'South_Sudan':'South Sudan',
                    'Syria':'Syrian Arab Republic',
                    'Taiwan':'Taiwan, Province of China',
                    'Iran':'Iran, Islamic Republic of',
                    'Tanzania':'Tanzania, United Republic of',
                    #'Tanzania':'United Republic of Tanzania',
                    'Eswatini':'Swaziland',
                    'Sri_Lanka':'Sri Lanka',
                    'UK':'United Kingdom',
                    'Russia':'Russian Federation',
                    'Palestine':'Palestine, State of',
                    'Moldova':'Moldova, Republic of',
                    'USA':'United States',
                    'UAE':'United Arab Emirates',
                    'Timor_Leste':'Timor-Leste',
                    'Laos':"Lao People's Democratic Republic",
                    'Trinidad_and_Tobago':'Trinidad and Tobago',
                    'Turks_and_Caicos_islands':'Turks and Caicos Islands',
                    'United_States_Virgin_Islands':'Virgin Islands, U.S.',
                    'United_Arab_Emirates':'United Arab Emirates',
                    'Venezuela':'Venezuela, Bolivarian Republic of'
                        }
    df.replace(translations, inplace=True)
def svenska_regioner(df):
    translations = {
      "Gotland":"Gotlands län",
      "Kalmar":"Kalmar län",
      "Blekinge":"Blekinge län",
      "Jämtland_Härjedalen":"Jämtlands län",
      "Västerbotten":"Västerbottens län",
      "Kronoberg":"Kronobergs län",
      "Västra_Götaland":"Västra Götalands län",
      "Värmland":"Värmlands län",
      "Dalarna":"Dalarnas län",
      "Sörmland":"Södermanlands län",
      "Jönköping":"Jönköpings län",
      "Östergötland":"Östergötlands län",
      "Stockholm":"Stockholms län",
      "Västmanland":"Västmanlands län",
      "Uppsala":"Uppsala län",
      "Gävleborg":"Gävleborgs län",
      "Västernorrland":"Västernorrlands län",
      "Örebro":"Örebro län",
      "Norrbotten":"Norrbottens län",
      "Halland":"Hallands län",
      "Skåne":"Skåne län"
                        }
    df.replace(translations, inplace=True)
def spain_communities(df):
    translations = {
        "AN":"Andalucia",
        "AR":"Aragon",
        "AS":"Asturias",
        "IB":"Baleares",
        "CN":"Canarias",
        "CB":"Cantabria",
        "CM":"Castilla-La Mancha",
        "CL":"Castilla-Leon",
        "CT":"Cataluña",
        "CE":"Ceuta",
        "EX":"Extremadura", 
        "GA":"Galicia",
        "RI":"La Rioja",
        "MD":"Madrid",
        "ML":"Melilla",
        "MC":"Murcia",
        "NC":"Navarra",
        "VC":"Valencia",
        "PV":"Pais Vasco"
                        }
    df.replace(translations, inplace=True)
def italy_regions(df):
    translations = {
        'Abruzzo':'Abruzzo',
        #"Valle d'Aosta":"Valle D'Aosta/Vallée D'Aoste", # Problems
        #'Apulia':'Puglia',
        'Puglia':'Puglia',
        'Basilicata':'Basilicata',
        'Calabria':'Calabria',
        'Campania':'Campania',
        'Emilia-Romagna':'Emilia-Romagna',
        #'Friuli Venezia Giulia':'Friuli Venezia Giulia',  # Also Udine
        'Lazio':'Lazio',
        'Liguria':'Liguria',
        'Lombardia':'Lombardia',
        'Marche':'Marche',
        'Molise':'Molise',
        'Piemonte':'Piemonte',
        'Sardegna':'Sardegna',
        'Sicilia':'Sicilia',
        #'P.A. Bolzano':'P.A. Bolzano',
        #'P.A. Trento':'Trentino-Alto Adige/Sudtirol', # PRoblens
        'Toscana':'Toscana',
        'Umbria':'Umbria',
        'Veneto':'Veneto',
        "Valle_d_Aosta":"Valle_D_Aosta_Vallee_D_Aoste",
        "#Valle_D_Aosta/Vallee_D_Aoste":"Valle_D_Aosta_Vallee_D_Aoste",
        "Friuli_Venezia_Giulia":"Friuli Venezia Giulia",
        "P.A._Bolzano":"P.A. Bolzano",
        "P.A._Trento":"Trentino-Alto_Adige_Sudtirol"
                        }
    df.replace(translations, inplace=True)
def spain_communities_nogaps(df):
    translations = {
        "AN":"Andalucia",
        "AR":"Aragon",
        "AS":"Asturias",
        "IB":"Baleares",
        "CN":"Canarias",
        "CB":"Cantabria",
        "CM":"Castilla-La_Mancha",
        "CL":"Castilla-Leon",
        "CT":"Cataluña",
        "CE":"Ceuta",
        "EX":"Extremadura", 
        "GA":"Galicia",
        "RI":"La_Rioja",
        "MD":"Madrid",
        "ML":"Melilla",
        "MC":"Murcia",
        "NC":"Navarra",
        "VC":"Valencia",
        "PV":"Pais_Vasco"
                        }
    df.replace(translations, inplace=True)

def fix_regions(df):
    translations = {
        "America-Alabama":"US-Alabama",
        "America-Alaska":"US-Alaska",
        "America-Arizona":"US-Arizona",
        "America-Arkansas":"US-Arkansas",
        "America-California":"US-California",
        "America-Colorado":"US-Colorado",
        "America-Connecticut":"US-Connecticut",
        "America-Delaware":"US-Delaware",
        "America-District_of_Columbia":"US-District of Columbia",
        "America-Florida":"US-Florida",
        "America-Georgia":"US-Georgia",
        "America-Hawaii":"US-Hawaii",
        "America-Idaho":"US-Idaho",
        "America-Illinois":"US-Illinois",
        "America-Indiana":"US-Indiana",
        "America-Iowa":"US-Iowa",
        "America-Kansas":"US-Kansas",
        "America-Kentucky":"US-Kentucky",
        "America-Louisiana":"US-Louisiana",
        "America-Maine":"US-Maine",
        "America-Maryland":"US-Maryland",
        "America-Massachusetts":"US-Massachusetts",
        "America-Michigan":"US-Michigan",
        "America-Minnesota":"US-Minnesota",
        "America-Mississippi":"US-Mississippi",
        "America-Missouri":"US-Missouri",
        "America-Montana":"US-Montana",
        "America-Nebraska":"US-Nebraska",
        "America-Nevada":"US-Nevada",
        "America-New_Hampshire":"US-New Hampshire",
        "America-New_Jersey":"US-New Jersey",
        "America-New_Mexico":"US-New Mexico",
        "America-New_York":"US-New York",
        "America-North_Carolina":"US-North Carolina",
        "America-North_Dakota":"US-North Dakota",
        "America-Ohio":"US-Ohio",
        "America-Oklahoma":"US-Oklahoma",
        "America-Oregon":"US-Oregon",
        "America-Pennsylvania":"US-Pennsylvania",
        "America-Rhode_Island":"US-Rhode Island",
        "America-South_Carolina":"US-South Carolina",
        "America-South_Dakota":"US-South Dakota",
        "America-Tennessee":"US-Tennessee",
        "America-Texas":"US-Texas",
        "America-Utah":"US-Utah",
        "America-Vermont":"US-Vermont",
        "America-Virginia":"US-Virginia",
        "America-Washington":"US-Washington",
        "America-West_Virginia":"US-West Virginia",
        "America-Wisconsin":"US-Wisconsin",
        "America-Wyoming":"US-Wyoming",
        "America-Puerto_Rico":"US-Puerto Rico",
        "Canada-Alberta":"CA-Alberta",
        "Canada-British_Columbia":"CA-British Columbia",
        "Canada-Calgary_Alberta":"CA-Alberta",
        "Canada-Edmonton_Alberta":"CA-Edmonton_Alberta",
        "Canada-London_ON":"CA-London_ON",
        "Canada-Manitoba":"CA-Manitoba",
        "Canada-New_Brunswick":"CA-New Brunswick",
        #"Canada-Newfoundland_and_Labrador":"CA-Newfoundland",
        "Canada-Newfoundland_and_Labrador":"CA-Newfoundland and Labrador",
        "Canada-Northwest_Territories":"CA-Northwest Territory",
        "Canada-Nova_Scotia":"CA-Nova Scotia",
        "Canada-Ontario":"CA-Ontario",
        "Canada-Prince_Edward_Island":"CA-Prince Edward Island",
        "Canada-Quebec":"CA-Quebec",
        "Canada-Saskatchewan":"CA-Saskatchewan",
        "Canada-Toronto_ON":"CA-Toronto",
        "Canada-Yukon":"CA-Yukon",
        "Canada-_Montreal_QC":"CA-Montreal",
        "Australia-Australia":"AU-Australia",
        "Australia-Australian_Capital_Territory":"AU-Australian Capital Territory",
        "Australia-New_South_Wales":"AU-New South Wales",
        "Australia-Northern_Territory":"AU-Northern_Territory",
        "Australia-Queensland":"AU-Queensland",
        "Australia-South_Australia":"AU-South Australia",
        "Australia-Tasmania":"AU-Tasmania",
        "Australia-Victoria":"AU-Victoria",
        "Australia-Western_Australia":"AU-Western Australia",
        "China-Anhui":"CH-Anhui",
        "China-Beijing":"CH-Beijing",
        "China-Chongqing":"CH-Chongqing",
        "China-Fujian":"CH-Fujian",
        "China-Gansu":"CH-Gansu",
        "China-Guangdong":"CH-Guangdong",
        "China-Guangxi":"CH-Guangxi",
        "China-Guizhou":"CH-Guizhou",
        "China-Hainan":"CH-Hainan",
        "China-Hebei":"CH-Hebei",
        "China-Heilongjiang":"CH-Heilongjiang",
        "China-Henan":"CH-Henan",
        "China-Hong_Kong":"CH-Hong Kong",
        "China-Hubei":"CH-Hubei",
        "China-Hunan":"CH-Hunan",
        "China-Inner_Mongolia":"CH-Inner Mongolia",
        "China-Jiangsu":"CH-Jiangsu",
        "China-Jiangxi":"CH-Jiangxi",
        "China-Jilin":"CH-Jilin",
        "China-Liaoning":"CH-Liaoning",
        "China-Macau":"CH-Macau",
        "China-Ningxia":"CH-Ningxia",
        "China-Qinghai":"CH-Qinghai",
        "China-Shaanxi":"CH-Shaanxi",
        "China-Shandong":"CH-Shandong",
        "China-Shanghai":"CH-Shanghai",
        "China-Shanxi":"CH-Shanxi",
        "China-Sichuan":"CH-Sichuan",
        "China-Tianjin":"CH-Tianjin",
        "China-Tibet":"CH-Tibet",
        "China-Xinjiang":"CH-Xinjiang",
        "China-Yunnan":"CH-Yunnan",
        "China-Zhejiang":"CH-Zhejiang"
     }
    df.replace(translations, inplace=True)



def get_r0_countries(input):

    ##Good place for geojson: https://github.com/codeforamerica/click_that_hood/tree/master/public/data
    # Swedish regions
    #https://github.com/deldersveld/topojson/countries/sweden/sweden-counties.json
    #url = 'https://raw.githubusercontent.com/python-visualization/folium/master/examples/data'
    #country_shapes = f'{url}/world-countries.json'

    #country_codes = csv.DictReader(open("country-threeletter.csv"))
    reader = csv.reader(open(os.getcwd()+'/data/country-threeletter.csv', 'r'))
    country_codes = {}
    for row in reader:
        k, v = row
        country_codes[k] = v
   
    #country_codes=pd.read_csv("country-threeletter.csv")
    r_df = pd.read_csv(input+"/merged_FHM_xls-R0-mobility.csv") # Sweden dataq
    r2_df = pd.read_csv(input+"/merged_ECDC-R0-mobility.csv") # ECDC data
    r3_df = pd.read_csv(input+"/merged_CCAA-R0-mobility.csv") # ECDC data
    r4_df = pd.read_csv(input+"/merged_province_JHS-R0-mobility.csv") # ECDC data
    r5_df = pd.read_csv(input+"/merged_italy-R0-mobility.csv") # ECDC data
    pop_df=pd.read_csv(os.getcwd()+"/data/regioner-befolknings.csv") # ECDC data

    r_df["datasource"]="Sweden"
    r2_df["datasource"]="ECDC"
    r3_df["datasource"]="Spain"
    r4_df["datasource"]="JHS-regions"
    r5_df["datasource"]="Italy"
    
    # We do need to find the last date where R0 is not NaN
    lastlist=[]
    firstlist=[]
    if (math.isnan(r_df.R0.tail(1).item())):
        last=r_df[r_df["R0"].notna()].index[-1]
    else:
        last=r_df.index[-1]
    last_r=r_df.date[last]
    first=r_df[r_df.R0.notna()].index[0]
    first_r=r_df.date[first]
    lastlist+=[FormatDateMerged(last_r)]
    firstlist+=[FormatDateMerged(first_r)]

    if (math.isnan(r2_df.R0.tail(1).item())):
        last=r2_df[r2_df["R0"].notna()].index[-1]
    else:
        last=r2_df.index[-1]
    last_r2=r2_df.date[last]
    first=r2_df[r2_df.R0.notna()].index[0]
    first_r2=r2_df.date[first]
    lastlist+=[FormatDateMerged(last_r2)]
    firstlist+=[FormatDateMerged(first_r2)]


    if (math.isnan(r3_df.R0.tail(1).item())):
        last=r3_df[r3_df["R0"].notna()].index[-1]
    else:
        last=r3_df.index[-1]
    last_r3=r3_df.date[last]
    first=r3_df[r3_df.R0.notna()].index[0]
    first_r3=r3_df.date[first]
    lastlist+=[FormatDateMerged(last_r3)]
    firstlist+=[FormatDateMerged(first_r3)]

    if (math.isnan(r4_df.R0.tail(1).item())):
        last=r4_df[r4_df["R0"].notna()].index[-1]
    else:
        last=r4_df.index[-1]
    last_r4=r4_df.date[last]
    first=r4_df[r4_df.R0.notna()].index[0]
    first_r4=r4_df.date[first]
    lastlist+=[FormatDateMerged(last_r4)]
    firstlist+=[FormatDateMerged(first_r4)]


    if (math.isnan(r5_df.R0.tail(1).item())):
        last=r5_df[r5_df["R0"].notna()].index[-1]
    else:
        last=r5_df.index[-1]
    last_r5=r5_df.date[last]
    first=r5_df[r5_df.R0.notna()].index[0]
    first_r5=r5_df.date[first]
    lastlist+=[FormatDateMerged(last_r5)]
    firstlist+=[FormatDateMerged(first_r5)]

    #print(firstlist,lastlist)
    #print(max(firstlist),min(lastlist))
    lastdate=min(lastlist)
    firstdate=max(firstlist)
    #sys.exit()
    #print (r_df)
    #R_df=r_df.loc[r_df.date==str(lastdate.date())]
    #R2_df=r2_df.loc[r2_df.date==str(lastdate.date())]
    #R3_df=r3_df.loc[r3_df.date==str(lastdate.date())]
    #R4_df=r4_df.loc[r4_df.date==str(lastdate.date())]
    #R5_df=r5_df.loc[r5_df.date==str(lastdate.date())]

    # in map-dates
    r_df['daterep']=r_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r2_df['daterep']=r2_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r3_df['daterep']=r3_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r4_df['daterep']=r4_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r5_df['daterep']=r5_df.apply(lambda x:FormatDateMerged(x.date), axis=1)


    if (r_df.loc[r_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
    if (r2_df.loc[r2_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
    if (r3_df.loc[r3_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
    if (r4_df.loc[r4_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
    if (r5_df.loc[r5_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
            

    # This deletes to much
            
    R_df=r_df.loc[   (r_df.daterep<=lastdate) & (  r_df.daterep >=firstdate)]
    R2_df=r2_df.loc[(r2_df.daterep<=lastdate) & ( r2_df.daterep >=firstdate)]
    R3_df=r3_df.loc[(r3_df.daterep<=lastdate) & ( r3_df.daterep >=firstdate)]
    R4_df=r4_df.loc[(r4_df.daterep<=lastdate) & ( r4_df.daterep >=firstdate)]
    R5_df=r5_df.loc[(r5_df.daterep<=lastdate) & ( r5_df.daterep >=firstdate)]

    R_df=r_df
    R2_df=r2_df
    R3_df=r3_df
    R4_df=r4_df
    R5_df=r5_df
    

    #print (R_df,r_df)


    #sys.exit()


    # We need to avoid some strange values
    maxval=3
    tiny=1.e-20
    #R_df['R0']=R_df['R0'].apply(lambda x:min(x,maxval))
    # also try with log
    R_df['LogR0']=R_df['R0'].apply(lambda x:(np.log2(max(x,tiny))))


    #R_df['code']=R_df['country'].apply(lambda x:(country_codes[x]))
    #print (SE_regioner)
    #print (world)
    #print (US_counties)
    # Good place for maps: https://github.com/codeforamerica/click_that_hood/tree/master/public/data
    #df = pd.read_csv("https://raw.githubusercontent.com/plotly/datasets/master/fips-unemp-16.csv",    dtype={"fips": str})

    pop_df=pop_df.rename(columns={
        'Namn':'country',
        'Pop2020':'popData2018'
        })
    sweden_df=pd.merge(R_df, pop_df, how='left', on=['country'])
    svenska_regioner(sweden_df)
    svenska_regioner(R_df)
    sweden_df["code"]=sweden_df["country"]


    map_names(R2_df)
    #print (R2_df)
    R2_df['code']=R2_df['country'].apply(lambda x:(country_codes[x]))
    #exclude_countries=["Spain","Sweden","United Stated of America"]
    #world_df=R2_df[ ((R2_df['country'] != "Spain" ) & (R2_df['country'] != "Sweden" ) & (R2_df['country'] != "United States of America" ))]
    #world_df=R2_df[ R2_df['country'] in exclude_countries)]
    # We skip this only when we make the maps
    #world_df=R2_df[ (  ( R2_df['country'] != "Spain" )
    #                 & ( R2_df['country'] != "Sweden" )
    #                 & ( R2_df['country'] != "United States" )
    #                 & ( R2_df['country'] != "Italy" )
    #                 & ( R2_df['country'] != "United States of America" )
    #                 & ( R2_df['country'] != "USA" )
    #                 & ( R2_df['country'] != "America" )
    #                 & ( R2_df['country'] != "China" )
    #                 & ( R2_df['country'] != "Canada" ))]
    world_df=R2_df


    spain_communities(R3_df)
    spain_df=pd.merge(R3_df, pop_df, how='left', on=['country'])
    spain_df["code"]=spain_df["country"]


    #print (R5_df.country.drop_duplicates())
    italy_regions(R5_df)
    italy_df=pd.merge(R5_df, pop_df, how='left', on=['country'])
    italy_df["code"]=italy_df["country"]
    #print (R5_df.country.drop_duplicates())


    fix_regions(R4_df)
    # We only include regons we have populations data for.
    regions_df=pd.merge(R4_df, pop_df, how='inner', on=['country'])
    regions_df["code"]=regions_df["country"]

    #common_cols = list(set.intersection(*(set(df.columns) for df in frames)))

    #print (regions_df)
    columns=["country","R0","sdR0","R0-FixSI","sdR0-FixSI","new_confirmed_cases",
                 "new_deaths","date","deaths","confirmed",
                 "Ratio","deaths_7days","cases_7days","popData2018",
                 "code","retail_and_recreation","grocery_and_pharmacy",
                 "parks","transit_stations","workplaces","residential","datasource"]
    concat_df=pd.concat([sweden_df[columns],spain_df[columns],italy_df[columns],regions_df[columns],world_df[columns]])
    
    #print (concat_df)

    concat_df.replace({
        "United States":"United States of America",
        "Côte d'Ivoire":"Ivory Coast",
        "Guinea-Bissau":"Guinea",
        "Congo":"Republic of the Congo",
        "Iran, Islamic Republic of":"Iran",
        "Syrian Arab Republic":"Syria",
        "Russian Federation":"Russia",
        "Lao People's Democratic Republic":"Laos",
        "Viet Nam":"Vietnam",
        "Taiwan, Province of China":"Taiwan",
        "Korea, Republic of":"South Korea",
        "Tanzania, United Republic of":"Tanzania",
        "Moldova, Republic of":"Moldova",
        "Serbia":"Republic of Serbia",
        "Macedonia, the former Yugoslav Republic of":"Macedonia",
        "Faroe Islands":"Faroarna",
        'Venezuela, Bolivarian Republic of':'Venezuela',
        'Moldova, Republic of':'Moldova',
        'Tanzania, United Republic of':'United Republic of Tanzania',
        "Newfoundland":"Newfoundland and Labrador",
                       }, inplace=True)

    return(concat_df,lastdate,firstdate)
    
